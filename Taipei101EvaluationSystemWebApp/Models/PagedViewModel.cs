﻿
namespace Taipei101EvaluationSystemWebApp.Models
{
    public class PagedViewModel<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
        public int TotalPage { get; set; }
        public T? Data { get; set; }
    }
}
