﻿using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemWebApp.Models
{
    public class AuthModel
    {
        public User? LogedInUser { get; set; }
        public string? Token { get; set; }
    }
}
