﻿using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace Taipei101EvaluationSystemWebApp.Models
{
    public class HttpClientModel
    {
        private readonly IConfiguration _configuration;

        public HttpClientModel(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<T> GetAsync<T>(string action, string? token) where T : class
        {
            string urlAddress = _configuration.GetValue<string>("Taipei101EvaluationSystemAPIURL") + action;
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            using (HttpResponseMessage response = await client.GetAsync(urlAddress))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    return null;

                using (HttpContent content = response.Content)
                {
                    var result = content.ReadAsStringAsync().Result;

                    var data = JsonConvert.DeserializeObject<T>(result);
                    return data;
                }
            }
        }

        public async Task<ResponseModel<T>> PostAsync<T>(string action, string? token, dynamic? data)
        {
            ResponseModel<T> responseModel = new ResponseModel<T>();
            try
            {
                string urlAddress = _configuration.GetValue<string>("Taipei101EvaluationSystemAPIURL") + action;
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                StringContent jsonContent = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                using (HttpResponseMessage response = await client.PostAsync(urlAddress, jsonContent))
                {
                    using (HttpContent content = response.Content)
                    {
                        var result = content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<T>>(result);
                    }
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
            }
            return responseModel;
        }

        public async Task<ResponseModel<T>> PutAsync<T>(string action, string? token, dynamic? data)
        {
            ResponseModel<T> responseModel = new ResponseModel<T>();
            try
            {
                string urlAddress = _configuration.GetValue<string>("Taipei101EvaluationSystemAPIURL") + action;
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                StringContent jsonContent = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                using (HttpResponseMessage response = await client.PutAsync(urlAddress, jsonContent))
                {
                    using (HttpContent content = response.Content)
                    {
                        var result = content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<T>>(result);
                    }
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
            }
            return responseModel;
        }

        public async Task<ResponseModel<T>> DeleteAsync<T>(string action, string? token)
        {
            ResponseModel<T> responseModel = new ResponseModel<T>();
            try
            {
                string apiURL = _configuration.GetValue<string>("Taipei101EvaluationSystemAPIURL") + action;
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                using (HttpResponseMessage response = await client.DeleteAsync(apiURL))
                {
                    using (HttpContent content = response.Content)
                    {
                        var result = content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<T>>(result);
                    }
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
            }
            return responseModel;
        }
    }
}
