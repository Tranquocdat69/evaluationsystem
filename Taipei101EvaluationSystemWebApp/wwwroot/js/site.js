﻿
toastr.options = {
    "positionClass": "toast-bottom-left",
}

function displayLoading() {
    $('.loading').show();
}
function hideLoading() {
    $('.loading').hide();
}
$(window).on('beforeunload', function () {
    displayLoading();
    setTimeout(function () {
        hideLoading();
    }, 3000)
});

function newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}