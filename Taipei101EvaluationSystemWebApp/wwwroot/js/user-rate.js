﻿toastr.options = {
    "positionClass": "toast-top-center",
}

function openRateModalToRate(id) {
    $.ajax({
        method: 'GET',
        url: '/UserRate/Rate?id=' + id,
        success: function (response) {
            $('#rate-modal-content').html(response);
            $('#rate-modal').modal('show');
        }
    })
};

function openRateModalToView(id) {
    $.ajax({
        method: 'GET',
        url: '/UserRate/Detail?userId=' + id,
        success: function (response) {
            $('#rate-modal-content').html(response);
            $('#rate-modal').modal('show');
        }
    })
};

function submitRate(username) {
    const content = $('#rate-content').val();
    const star = $('input[name="' + username + '"]:checked').val();

    if (!content) {
        $('#rate-content').focus();
        toastr.warning('Vui lòng nhập ý kiến đánh giá');
        return;
    }
    if (typeof(star) == 'undefined') {
        toastr.warning('Vui lòng đánh giá sao');
        return;
    }

    const ratedUserId = $('#rate-for').val();
    const userRate = {
        RateFor: ratedUserId,
        StarNumber: star,
        RatedContent: content
    };

    $.ajax({
        method: 'POST',
        url: '/UserRate/Insert',
        data: userRate,
        success: function (response) {
            if (response.status == 1) {
                toastr.success('Đánh giá thành công!');
                setTimeout(() => {
                    location.reload();
                }, 1500)
            }
            else if (response.status == 0) {
                toastr.error('Người dùng đã được đánh giá!');
            }
            else {
                toastr.error(response.message);
                console.log(response)
            }
        }
    });
}