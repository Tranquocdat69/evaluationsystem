﻿toastr.options = {
    "positionClass": "toast-top-center",
}

function uploadAvatar(self, uploadsFolder) {
    const file = $(self)[0].files[0];
    if (file) {
        let formData = new FormData();
        formData.append("file", file);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                let div = `<div id="progress-bar-container" class="progress w-100">
                                              <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>
                                           </div>`
                $("#upload-avatar-container").append(div);
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $("#progress-bar").css('width', percentComplete + '%');
                        $("#progress-bar").text(percentComplete + '%');
                        if (percentComplete === 100) {
                            $("#progress-bar-container").remove();
                        }
                    }
                }, false);

                return xhr;
            },
            method: 'POST',
            url: '/File/Upload?folder=avatars',
            data: formData,
            contentType: false,
            processData: false,
            cache: false,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Avatar uploaded!');
                    $('#avatar-image').attr('src', uploadsFolder + response.data);
                    $('#avatar-url').val(response.data);
                }
                else {
                    console.log(response);
                }
            }
        })
    }
}

function updateProfile(id) {
    const avatar = $('#avatar-url').val();
    const name = $('#name').val().trim();

    if (name) {
        const user = {
            Id: id,
            Name: name,
            Avatar: avatar
        };
        $.ajax({
            method: 'PUT',
            url: '/User/Update',
            data: user,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Cập nhật thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Thành viên đã tồn tại!');
                }
                else if (response.status == 401) {
                    toastr.error('Bạn không có quyền thực hiện chức năng này!');
                }
                else {
                    console.log(response);
                    toastr.error(response.Message);
                }
            }
        });
    }
    else {
        toastr.warning('Vui lòng nhập họ và tên!');
    }
};

function changePassword(id) {
    if (id) {
        const oldPassword = $('#old-password').val();
        const newPassword = $('#new-password').val();
        const confirmNewPassword = $('#confirm-new-password').val();

        if (typeof oldPassword === 'undefined' || typeof newPassword === 'undefined' || typeof confirmNewPassword === 'undefined' || oldPassword === '' || newPassword === '' || confirmNewPassword === '') {
            toastr.warning('Vui lòng nhập đầy đủ thông tin!');
            return;   
        }

        $.ajax({
            method: 'GET',
            url: '/User/HashToMD5?oldPassword=' + oldPassword,
            success: function (response) {
                const oldPasswordMD5 = response.toLowerCase();
                $.ajax({
                    method: 'GET',
                    url: '/User/GetById?id=' + id,
                    success: function (response) {
                        if (oldPasswordMD5 !== response.password.toLowerCase()) {
                            toastr.warning('Mật khẩu cũ không chính xác!');
                            $('#old-password').focus();
                            return;
                        };

                        if (newPassword === oldPassword) {
                            toastr.warning('Mật khẩu mới phải khác mật khẩu cũ!');
                            $('#new-password').focus();
                            return;
                        }

                        if (newPassword !== confirmNewPassword) {
                            toastr.warning('Xác nhận mật khẩu không khớp!');
                            $('#confirm-new-password').focus();
                            return;
                        }

                        const user = {
                            Id: id,
                            Password: newPassword,
                        };

                        $.ajax({
                            method: 'PUT',
                            url: '/User/UpdatePassword',
                            data: user,
                            success: function (response) {
                                if (response.status == 1) {
                                    toastr.success('Cập nhật thành công!');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1500)
                                }
                                else if (response.status == 0) {
                                    toastr.error('Thành viên đã tồn tại!');
                                }
                                else if (response.status == 401) {
                                    toastr.error('Bạn không có quyền thực hiện chức năng này!');
                                }
                                else {
                                    console.log(response);
                                    toastr.error(response.Message);
                                }
                            }
                        });
                    }
                });
            }
        });
    }
}