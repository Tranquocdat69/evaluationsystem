﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class AnswerController : Controller
    {
        private readonly IConfiguration _configuration;
        public AnswerController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<JsonResult> Insert(UserContestAnswer userContestAnswer)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var responseModel = await httpClient.PostAsync<dynamic>($"/Answer/Insert", logedInUser.Token, userContestAnswer);
            return Json(responseModel);
        }

        public async Task<JsonResult> Delete(UserContestAnswer userContestAnswer)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var responseModel = await httpClient.PostAsync<dynamic>($"/Answer/Delete", logedInUser.Token, userContestAnswer);
            return Json(responseModel);
        }
    }
}
