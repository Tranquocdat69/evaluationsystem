﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class LoginController : Controller
    {
        IConfiguration _configuration;

        public LoginController(IConfiguration configuration) {
            _configuration= configuration;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(User user)
        {
            var utilities = new Utilities();
            HttpClientModel httpClientModel = new HttpClientModel(_configuration);

            var responseModel = await httpClientModel.PostAsync<AuthModel>("/User/Login", null,user);

            if (responseModel.Status == 1)
            {
                var logedInUser = (AuthModel)responseModel.Data;
                var stringAuthModel = JsonConvert.SerializeObject(logedInUser);
                HttpContext.Session.SetString(Constants.SESSION_NAME, stringAuthModel);
                return RedirectToAction("Index", "Home");
            }
            else if (responseModel.Status == -1)
            {
                ViewBag.Notification = "Mật khẩu không chính xác!";
            }
            else if (responseModel.Status == 0)
            {
                ViewBag.Notification = "Tài khoản không tồn tại!";
            }

            return View("Index");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }
    }
}
