﻿using Azure;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class ContestController : BaseController
    {
        private readonly IConfiguration _configuration;
        public ContestController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Index(string? contestName, int pageIndex = 1, int pageSize = 24)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var contests = await httpClient.GetAsync<PagedViewModel<List<UserContestViewModel>>>($"/Contest/GetPaging?contestName={contestName}&pageIndex={pageIndex}&pageSize={pageSize}", logedInUser?.Token);
            
            if (contests is null)
                return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền xem danh sách bài thi" });

            ViewBag.ContestName = contestName;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(contests);
        }

        public async Task<PartialViewResult> End(Guid id)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var responseModel = await httpClient.GetAsync<ResponseModel<UserContestViewModel>>($"/Contest/End?id={id}", logedInUser?.Token);
            return PartialView(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Result(Guid userContestId)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var userContestHasNoIsCorrect = await httpClient.GetAsync<UserContestHasNoIsCorrect>($"/Question/GetAll?userContestId={userContestId}", logedInUser?.Token);

            return PartialView(userContestHasNoIsCorrect);
        }
    }
}
