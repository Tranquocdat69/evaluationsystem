﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class QuestionController : Controller
    {
        private readonly IConfiguration _configuration;
        public QuestionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Index(Guid constestId, Guid userContestId)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);

            var userContest = await httpClient.GetAsync<UserContest>($"/userContests/{userContestId}", logedInUser?.Token);
            if (userContest.StartedTime is null)
            {
                var listContestDetail = await httpClient.GetAsync<List<ContestDetail>>($"/contests/{constestId}/contestDetails", logedInUser?.Token);
                var listRandomQuestion = await httpClient.PostAsync<List<Question>>($"/Question/GetRandom", logedInUser?.Token, listContestDetail);

                List<UserContestQuestion> userContestQuestions = new List<UserContestQuestion>();
                listRandomQuestion.Data.ForEach(x => userContestQuestions.Add(new UserContestQuestion() { UserContestId = userContestId, QuestionId = x.Id }));

                if (userContestQuestions.Count > 0)
                {
                    UserContestQuestionsViewModel userContestQuestionsViewModel = new UserContestQuestionsViewModel() { UserContestQuestions = userContestQuestions };
                    var result = await httpClient.PostAsync<ResponseModel>("/userContestQuestions", logedInUser?.Token, userContestQuestionsViewModel);
                }
            }

            var answers = await httpClient.GetAsync<List<UserContestAnswer>>($"/Answer/GetAnswers?userContestId={userContestId}", logedInUser?.Token);
            var questions = await httpClient.GetAsync<UserContestHasNoIsCorrect>($"/Question/GetAll?userContestId={userContestId}", logedInUser?.Token);

            if (answers is null)
                return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền xem câu trả lời" });

            if (questions is null)
                return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền xem câu hỏi" });

            ViewBag.Answers = answers;

            if (questions.UserContest is null)
                return RedirectToAction("Error404", "Error");

            if (questions.UserContest.EndedTime is not null)
                return RedirectToAction("Error404", "Error");

            if (questions.UserContest?.StartedTime is null)
            {
                var responseModel = await httpClient.GetAsync<ResponseModel<UserContestViewModel>>($"/Contest/Start?id={userContestId}", logedInUser?.Token);
                if (responseModel is null)
                    return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền bắt đầu bài thi" });
                
                questions.UserContest.StartedTime = responseModel.Data.StartedTime;
            }
           
            return View(questions);
        }
    }
}
