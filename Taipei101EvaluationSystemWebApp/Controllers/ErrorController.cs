﻿using Microsoft.AspNetCore.Mvc;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Error401(string message)
        {
            ViewBag.Message = message;
            return View("401");
        }
        
        public IActionResult Error404(string message)
        {
            ViewBag.Message = message;
            return View("404");
        }
    }
}
