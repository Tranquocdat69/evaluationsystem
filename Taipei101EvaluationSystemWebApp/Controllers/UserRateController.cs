﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class UserRateController : BaseController
    {
        private readonly IConfiguration _configuration;

        public UserRateController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Index(int pageIndex = 1, int pageSize = 12)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);

            var pageViewModel = await httpClient.GetAsync<PagedViewModel<List<UserToRateModel>>>($"/User/GetUsersToRatePaging?departmentId={logedInUser?.LogedInUser?.DepartmentId}&username={logedInUser?.LogedInUser?.Username}&pageIndex={pageIndex}&pageSize={pageSize}", logedInUser?.Token);
            
            if (pageViewModel is null)
                return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền xem danh sách thành viên" });

            if (pageViewModel.Data is not null)
            {
                var allUserRate = await httpClient.GetAsync<List<UserRateViewModel>>($"/UserRate/GetAll?departmentId={logedInUser?.LogedInUser?.DepartmentId}&userId={logedInUser?.LogedInUser?.Id}", logedInUser?.Token);
                
                if (allUserRate is null)
                    return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền đánh giá thành viên" });
                
                foreach (var userToRate in pageViewModel.Data)
                {
                    UserRateViewModel? userRateByRateFor = allUserRate.Where(x => x.RateFor == userToRate.Id).FirstOrDefault();
                    if (userRateByRateFor is not null)
                    {
                        userToRate.IsRated = true;
                    }
                }
            }

            ViewBag.UploadsFolder = _configuration.GetValue<string>("UploadsFolder");

            return View(pageViewModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Rate(Guid id)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var user = await httpClient.GetAsync<User>($"/User/GetById?id={id}", logedInUser?.Token);
            return PartialView(user);
        }
        
        [HttpGet]
        public async Task<PartialViewResult> Detail(Guid userId)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var userRate = await httpClient.GetAsync<UserRateViewModel>($"/UserRate/GetDetail?rateBy={logedInUser.LogedInUser.Id}&rateFor={userId}", logedInUser?.Token);
            return PartialView(userRate);
        }
        
        [HttpPost]
        public async Task<JsonResult> Insert(UserRate userRate)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);

            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);    
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);

            userRate.RateBy = logedInUser.LogedInUser.Id;
            ResponseModel<dynamic>  responseModel = await httpClient.PostAsync<dynamic>($"/UserRate/Insert", logedInUser?.Token, userRate);
            return Json(responseModel);
        }
    }
}