﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class FileController : Controller
    {
        private readonly IConfiguration _configuration;

        public FileController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 4294967295)]
        [RequestSizeLimit(4294967295)]
        public async Task<JsonResult> Upload(IFormFile file, string? folder)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string urlAddress = _configuration.GetValue<string>("Taipei101EvaluationSystemAPIURL") + "/File/Upload?folder=" + folder;
                using var client = new HttpClient();

                using (var formData = new MultipartFormDataContent())
                {
                    formData.Add(new StreamContent(file.OpenReadStream()), "file", file.FileName);
                    using (HttpResponseMessage response = await client.PostAsync(urlAddress, formData))
                    {
                        using (HttpContent content = response.Content)
                        {
                            var result = content.ReadAsStringAsync().Result;
                            responseModel = JsonConvert.DeserializeObject<ResponseModel>(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return Json(responseModel);
        }
    }
}
