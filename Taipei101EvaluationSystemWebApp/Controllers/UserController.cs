﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class UserController : BaseController
    {
        private readonly IConfiguration _configuration;

        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Index()
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);
            var result = await httpClient.GetAsync<UserViewModel>($"/User/GetByUserName?userName={logedInUser?.LogedInUser?.Username}", logedInUser?.Token);

            if (result is null)
                return RedirectToAction("Error401", "Error", routeValues: new { message = "Bạn không có quyền xem thông tin cá nhân" });

            ViewBag.UploadsFolder = _configuration.GetValue<string>("UploadsFolder");

            return View(result);
        }

        [HttpGet]
        public async Task<JsonResult> GetById(Guid id)
        {
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);

            var user = await httpClient.GetAsync<User>($"/User/GetById?id={id}", logedInUser?.Token);

            return Json(user);
        }
        
        [HttpGet]
        public JsonResult HashToMD5(string oldPassword) => Json(new Utilities().CreateMD5(oldPassword));
        
        [HttpPut]
        public async Task<JsonResult> Update(User user)
        {
            user.UpdatedTime = DateTime.Now;
           
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);

            var responseModel = await httpClient.PutAsync<dynamic>($"/User/UpdateProfile", logedInUser?.Token, user);

            return Json(responseModel);
        }

        [HttpPut]
        public async Task<JsonResult> UpdatePassword(User user)
        {
            user.UpdatedTime = DateTime.Now;
           
            HttpClientModel httpClient = new HttpClientModel(_configuration);
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var logedInUser = JsonConvert.DeserializeObject<AuthModel>(session);

            var responseModel = await httpClient.PutAsync<dynamic>($"/User/UpdatePassword", logedInUser?.Token, user);

            return Json(responseModel);
        }
    }
}
