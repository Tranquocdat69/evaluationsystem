﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Taipei101EvaluationSystemWebApp.Models;

namespace Taipei101EvaluationSystemWebApp.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}