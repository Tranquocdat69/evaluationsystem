﻿using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository
{
    public interface IContestDetailRepository
    {
        public Task<List<ContestDetail>> GetByContestId(Guid contestId);

    }
}
