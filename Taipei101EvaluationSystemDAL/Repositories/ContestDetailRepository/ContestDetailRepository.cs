﻿using Dapper;
using System.Data;
using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository
{
    public class ContestDetailRepository : IContestDetailRepository
    {
        private readonly DapperContext _context;
        public ContestDetailRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<List<ContestDetail>> GetByContestId(Guid contestId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContestId", contestId, DbType.Guid, ParameterDirection.Input);

                var result = await connection.QueryAsync<ContestDetail>("SP_ContestDetail_GetByContestId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }
    }
}
