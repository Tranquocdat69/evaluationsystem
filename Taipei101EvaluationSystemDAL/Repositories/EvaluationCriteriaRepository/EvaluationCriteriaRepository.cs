﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.EvaluationCriteriaRepository
{
    public class EvaluationCriteriaRepository : IEvaluationCriteriaRepository
    {
        private readonly DapperContext _context;
        public EvaluationCriteriaRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_EvaluationCriteria_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<List<EvaluationCriteria>> GetAll(int? level)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Level", level, DbType.Int32, ParameterDirection.Input);
                var result = await connection.QueryAsync<EvaluationCriteria>("SP_EvaluationCriteria_GetAll", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<EvaluationCriteria> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<EvaluationCriteria>("SP_EvaluationCriteria_GetById", parameters, commandType: CommandType.StoredProcedure);
                return result.SingleOrDefault();
            }
        }

        public async Task<PagedViewModel> GetPaging(string? criteria, int? level, int pageIndex, int pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();
                var parameters = new DynamicParameters();
                parameters.Add("@Criteria", criteria, DbType.String, ParameterDirection.Input);
                parameters.Add("@Level", level, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                var result = await connection.QueryAsync<EvaluationCriteriaViewModel>("SP_EvaluationCriteria_GetPaging", parameters, commandType: CommandType.StoredProcedure);
                var departments = result.ToList();
                responseModel.PageIndex = pageIndex;
                responseModel.PageSize = pageSize;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = departments;
                return responseModel;
            }
        }

        public async Task<ResponseModel> Insert(EvaluationCriteria evaluationCriteria)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Criteria", evaluationCriteria.Criteria, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Level", evaluationCriteria.Level, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@Order", evaluationCriteria.Order, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@CreatedBy", evaluationCriteria.CreatedBy, DbType.Guid, ParameterDirection.Input);
                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_EvaluationCriteria_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<EvaluationCriteria>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<ResponseModel> Update(EvaluationCriteria evaluationCriteria)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", evaluationCriteria.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Criteria", evaluationCriteria.Criteria, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Level", evaluationCriteria.Level, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@Order", evaluationCriteria.Order, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@UpdatedBy", evaluationCriteria.UpdatedBy, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", evaluationCriteria.UpdatedTime, DbType.DateTime, ParameterDirection.Input);
                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_EvaluationCriteria_Update", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<EvaluationCriteria>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }
    }
}
