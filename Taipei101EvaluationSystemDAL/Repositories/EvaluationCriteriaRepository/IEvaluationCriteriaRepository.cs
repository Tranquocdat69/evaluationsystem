﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.EvaluationCriteriaRepository
{
    public interface IEvaluationCriteriaRepository
    {
        public Task<PagedViewModel> GetPaging(string? criteria, int? level, int pageIndex, int pageSize);
        public Task<EvaluationCriteria> GetById(Guid id);
        public Task<List<EvaluationCriteria>> GetAll(int? level);
        public Task<ResponseModel> Insert(EvaluationCriteria evaluationCriteria);
        public Task<ResponseModel> Update(EvaluationCriteria evaluationCriteria);
        public Task<ResponseModel> Delete(Guid id);
    }
}
