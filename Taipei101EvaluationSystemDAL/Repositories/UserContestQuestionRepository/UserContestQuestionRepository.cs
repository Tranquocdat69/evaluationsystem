﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository
{
    public class UserContestQuestionRepository : IUserContestQuestionRepository
    {
        private readonly DapperContext _context;
        public UserContestQuestionRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> InsertMany(UserContestQuestionsViewModel userContestQuestionsViewModel)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    XElement? userContestQuestionsXML = null;
                    if (userContestQuestionsViewModel.UserContestQuestions?.Count > 0)
                    {
                        userContestQuestionsXML = new XElement("UserContestQuestions",
                            from userContestQuestion in userContestQuestionsViewModel.UserContestQuestions
                            select new XElement("UserContestQuestion",
                                new XElement("UserContestId", userContestQuestion.UserContestId),
                                new XElement("QuestionId", userContestQuestion.QuestionId)
                            )
                        );
                    }
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserContestQuestionsXML", userContestQuestionsXML?.ToString(), DbType.String, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_UserContestQuestion_InsertMany", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<ResponseModel> Delete(Guid userContestId, Guid questionId)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserContestId", userContestId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@QuestionId", questionId, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_UserContestQuestion_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<UserContestDetailModel> GetByUserContestId(Guid userContestId, string? questionContent)
        {
            using (var connection = _context.CreateConnection())
            {
                var userContestDetailModel = new UserContestDetailModel();
                var parameters = new DynamicParameters();
                parameters.Add("@UserContestId", userContestId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@QuestionContent", questionContent, DbType.String, ParameterDirection.Input);

                using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserContestQuestion_GetByUserContestId", parameters, commandType: CommandType.StoredProcedure))
                {
                    var userContestResult = await queryMultiple.ReadAsync<UserContestViewModel>();
                    userContestDetailModel.UserContest = userContestResult.SingleOrDefault();
                    if (userContestDetailModel.UserContest != null)
                    {
                        var questionViewModelsResult = await queryMultiple.ReadAsync<QuestionViewModel>();
                        userContestDetailModel.Questions = questionViewModelsResult.ToList();

                        if (userContestDetailModel.Questions?.Count > 0)
                        {
                            var answersResult = await queryMultiple.ReadAsync<Answer>();
                            var answers = answersResult.ToList();
                            foreach (var questionViewModel in userContestDetailModel.Questions)
                            {
                                questionViewModel.Answers = answers.Where(x => x.QuestionId == questionViewModel.Id).ToList();
                            }
                        }
                    }
                }
                return userContestDetailModel;
            }
        }

        public async Task<UserContestHasNoIsCorrect> GetAllHasNoIsCorrect(Guid userContestId, Guid userId)
        {
            using (var connection = _context.CreateConnection())
            {
                var userContestDetailModel = new UserContestHasNoIsCorrect();
                var parameters = new DynamicParameters();
                parameters.Add("@UserContestId", userContestId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@UserId", userId, DbType.Guid, ParameterDirection.Input);

                using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserContestQuestion_GetAllHasNoIsCorrect", parameters, commandType: CommandType.StoredProcedure))
                {
                    var userContestResult = await queryMultiple.ReadAsync<UserContestViewModel>();
                    userContestDetailModel.UserContest = userContestResult.SingleOrDefault();

                    if (userContestDetailModel.UserContest != null && userContestDetailModel.UserContest.EndedTime == null)
                    {
                        var questionViewModelsResult = await queryMultiple.ReadAsync<QuestionHasNoIsCorrect>();
                        userContestDetailModel.Questions = questionViewModelsResult.ToList();

                        if (userContestDetailModel.Questions?.Count > 0)
                        {
                            var answersResult = await queryMultiple.ReadAsync<AnswerHasNoIsCorrect>();
                            var answers = answersResult.ToList();
                            foreach (var questionViewModel in userContestDetailModel.Questions)
                            {
                                questionViewModel.Answers = answers.Where(x => x.QuestionId == questionViewModel.Id).ToList();
                            }
                        }
                    }
                }
                return userContestDetailModel;
            }
        }
    }
}
