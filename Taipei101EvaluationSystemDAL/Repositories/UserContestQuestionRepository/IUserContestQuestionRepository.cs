﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository
{
    public interface IUserContestQuestionRepository
    {
        public Task<UserContestDetailModel> GetByUserContestId(Guid userContestId, string? questionContent);
        public Task<UserContestHasNoIsCorrect> GetAllHasNoIsCorrect(Guid userContestId, Guid userId);
        public Task<ResponseModel> InsertMany(UserContestQuestionsViewModel userContestQuestionsViewModel);
        public Task<ResponseModel> Delete(Guid userContestId, Guid questionId);
    }
}
