﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.DepartmentRepository
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly DapperContext _context;
        public DepartmentRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_Department_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<Department> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<Department>("SP_Department_GetById", parameters, commandType: CommandType.StoredProcedure);
                return result.SingleOrDefault();
            }
        }

        public async Task<PagedViewModel> GetPaging(string? name, int pageIndex, int pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();
                var parameters = new DynamicParameters();
                parameters.Add("@Name", name, DbType.String, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                var result = await connection.QueryAsync<DepartmentViewModel>("SP_Department_GetPaging", parameters, commandType: CommandType.StoredProcedure);
                var departments = result.ToList();
                responseModel.PageIndex = pageIndex;
                responseModel.PageSize = pageSize;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = departments;
                return responseModel;
            }
        }

        public async Task<List<Department>> GetAll()
        {
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<Department>("SP_Department_GetAll", null, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<ResponseModel> Insert(Department department)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Name", department.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@CreatedBy", department.CreatedBy, DbType.Guid, ParameterDirection.Input);
                    

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Department_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<Department>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }

        public async Task<ResponseModel> Update(Department department)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", department.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Name", department.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@UpdatedBy", department.UpdatedBy, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", department.UpdatedTime, DbType.DateTime, ParameterDirection.Input);
                    parameters.Add("@Status", department.Status, DbType.Boolean, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Department_Update", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<Department>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }
    }
}
