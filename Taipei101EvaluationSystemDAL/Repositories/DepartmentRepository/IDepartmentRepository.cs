﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.DepartmentRepository
{
    public interface IDepartmentRepository
    {
        public Task<PagedViewModel> GetPaging(string? name, int pageIndex, int pageSize);
        public Task<Department> GetById(Guid id);
        public Task<List<Department>> GetAll();
        public Task<ResponseModel> Insert(Department department);
        public Task<ResponseModel> Update(Department department);
        public Task<ResponseModel> Delete(Guid id);
    }
}
