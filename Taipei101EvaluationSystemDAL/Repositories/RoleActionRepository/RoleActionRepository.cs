﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.RoleActionRepository
{
    public class RoleActionRepository : IRoleActionRepository
    {
        private readonly DapperContext _context;
        public RoleActionRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Insert(RoleAction roleAction)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@RoleId", roleAction.RoleId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@ActionId", roleAction.ActionId, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_RoleAction_Insert", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.Single();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<ResponseModel> InsertMany(RoleActionsViewModel roleActionsViewModel)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    XElement? actionIdsXML = null;
                    if (!string.IsNullOrEmpty(roleActionsViewModel.ActionIds))
                    {
                        var splitedActionIds = roleActionsViewModel.ActionIds.Split(',');

                        actionIdsXML = new XElement("Actions",
                            from actionId in splitedActionIds
                            select new XElement("Action",
                                new XElement("ActionId", actionId.Trim())
                            )
                        );
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@RoleId", roleActionsViewModel.RoleId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@ActionIdsXML", actionIdsXML?.ToString(), DbType.String, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_RoleAction_InsertMany", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.Single();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }
    }
}
