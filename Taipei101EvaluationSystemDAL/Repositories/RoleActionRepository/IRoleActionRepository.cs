﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.RoleActionRepository
{
    public interface IRoleActionRepository
    {
        public Task<ResponseModel> InsertMany(RoleActionsViewModel roleActionsViewModel);
        public Task<ResponseModel> Insert(Entities.RoleAction roleAction);
    }
}
