﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserRepository
{
    public class UserRepository : IUserRepository
    {
        private readonly DapperContext _context;
        public UserRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_User_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.Single();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<User> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<User>("SP_User_GetById", parameters, commandType: CommandType.StoredProcedure);
                return result.Single();
            }
        }

        public async Task<UserViewModel> GetDetailByUsername(string userName)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Username", userName, DbType.String, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserViewModel>("SP_User_GetDetailByUsername", parameters, commandType: CommandType.StoredProcedure);
                return result.Single();
            }
        }

        public async Task<PagedViewModel> GetUsersToRatePaging(Guid departmentId, string username, int pageIndex, int pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();

                var parameters = new DynamicParameters();
                parameters.Add("@DepartmentId", departmentId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@Username", username, DbType.String, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);

                var result = await connection.QueryAsync<UserToRateModel>("SP_User_GetToRatePaging", parameters, commandType: CommandType.StoredProcedure);

                responseModel.PageIndex = pageIndex;
                responseModel.PageSize = pageSize;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = result.ToList();

                return responseModel;
            }
        }

        public async Task<List<User>> GetAll()
        {
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<Entities.User>("SP_User_GetAll", null, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<PagedViewModel> GetPaging(string? searchingText, Guid? departmentId, int pageIndex, int pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();

                var parameters = new DynamicParameters();
                parameters.Add("@DepartmentId", departmentId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@Username", searchingText, DbType.String, ParameterDirection.Input);
                parameters.Add("@Name", searchingText, DbType.String, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                var result = await connection.QueryAsync<UserViewModel>("SP_User_GetPaging", parameters, commandType: CommandType.StoredProcedure);
                var users = result.ToList();

                responseModel.PageIndex = pageIndex;
                responseModel.PageSize = pageSize;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = users;
                return responseModel;
            }
        }

        public async Task<ResponseModel> Insert(User user)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@DepartmentId", user.DepartmentId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Username", user.Username, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Password", user.Password, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Name", user.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Avatar", user.Avatar, DbType.String, ParameterDirection.Input);
                    parameters.Add("@RoleId", user.RoleId, DbType.Guid, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_User_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserViewModel>();
                            responseModel.Data = userViewModelResult.Single();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }

        public async Task<ResponseModel> Login(User user)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Username", user.Username, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Password", user.Password, DbType.String, ParameterDirection.Input);
                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_User_Login", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var responseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = responseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserViewModel>();
                            var userViewModel = userViewModelResult.Single();
                            responseModel.Data = userViewModel;
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<ResponseModel> Update(User user)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", user.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@DepartmentId", user.DepartmentId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Password", user.Password, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Name", user.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Avatar", user.Avatar, DbType.String, ParameterDirection.Input);
                    parameters.Add("@RoleId", user.RoleId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", user.UpdatedTime, DbType.DateTime, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_User_Update", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserViewModel>();
                            responseModel.Data = userViewModelResult.Single();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }
        
        public async Task<ResponseModel> UpdateProfile(User user)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", user.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Name", user.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Avatar", user.Avatar, DbType.String, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", user.UpdatedTime, DbType.DateTime, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_User_UpdateProfile", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserViewModel>();
                            responseModel.Data = userViewModelResult.Single();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }

        public async Task<ResponseModel> UpdatePassword(User user)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", user.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Password", user.Password, DbType.String, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", user.UpdatedTime, DbType.DateTime, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_User_UpdatePassword", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserViewModel>();
                            responseModel.Data = userViewModelResult.Single();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }
    }
}