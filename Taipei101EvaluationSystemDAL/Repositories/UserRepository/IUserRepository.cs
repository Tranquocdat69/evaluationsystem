﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserRepository
{
    public interface IUserRepository
    {
        public Task<ResponseModel> Login(Entities.User user);
        public Task<ResponseModel> Insert(Entities.User user);
        public Task<ResponseModel> Update(Entities.User user);
        public Task<ResponseModel> UpdateProfile(Entities.User user);
        public Task<ResponseModel> UpdatePassword(Entities.User user);
        public Task<PagedViewModel> GetPaging(string? searchingText, Guid? departmentId, int pageIndex, int pageSize);
        public Task<List<User>> GetAll();
        public Task<User> GetById(Guid id);
        public Task<UserViewModel> GetDetailByUsername(string userName);
        public Task<PagedViewModel> GetUsersToRatePaging(Guid departmentId, string userName, int pageIndex, int pageSize);
        public Task<ResponseModel> Delete(Guid id);
    }
}
