﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.RoleRepository
{
    public class RoleRepository : IRoleRepository
    {
        private readonly DapperContext _context;
        public RoleRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_Role_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.Single();
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<List<RoleViewModel>> GetAll(string? sort)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Sort", sort?.ToUpper(), DbType.String, ParameterDirection.Input);
                var roles = await connection.QueryAsync<RoleViewModel>("SP_Role_GetAll", parameters, commandType: CommandType.StoredProcedure);
                return roles.ToList();
            }
        }

        public async Task<Role> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<Entities.Role>("SP_Role_GetById", parameters, commandType: CommandType.StoredProcedure);
                return result.SingleOrDefault();
            }
        }

        public async Task<ResponseModel> Insert(Role role)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@RoleName", role.RoleName, DbType.String, ParameterDirection.Input);
                    parameters.Add("@CreatedBy", role.CreatedBy, DbType.Guid, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Role_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var roleResult = await queryMultiple.ReadAsync<Entities.Role>();
                            responseModel.Data = roleResult.Single();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<ResponseModel> Update(Role role)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", role.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@RoleName", role.RoleName, DbType.String, ParameterDirection.Input);
                    parameters.Add("@UpdateTime", role.UpdatedTime, DbType.DateTime, ParameterDirection.Input);
                    parameters.Add("@UpdatedBy", role.UpdatedBy, DbType.Guid, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Role_Update", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.Single();
                        if (responseModel.Status == 1)
                        {
                            var roleResult = await queryMultiple.ReadAsync<Entities.Role>();
                            responseModel.Data = roleResult.Single();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }
    }
}
