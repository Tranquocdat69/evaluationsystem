﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.RoleRepository
{
    public interface IRoleRepository
    {
        public Task<ResponseModel> Insert(Entities.Role role);
        public Task<ResponseModel> Update(Entities.Role role);
        public Task<ResponseModel> Delete(Guid id);
        public Task<Entities.Role> GetById(Guid id);
        public Task<List<RoleViewModel>> GetAll(string? sort);
    }
}
