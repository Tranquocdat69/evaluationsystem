﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.QuestionRepository
{
    public interface IQuestionRepository
    {
        public Task<ContestDetailModel> GetByContestId(Guid id, string? questionContent);
        public Task<ResponseModel> InsertMany(QuestionsListViewModel questionsListViewModel, Guid createdBy);
        public Task<QuestionViewModel> GetById(Guid id);
        public Task<ResponseModel> Update(QuestionViewModel questionViewModel);
        public Task<ResponseModel> Delete(Guid id);
        public Task<List<Question>> Search(Guid? userContestId, Guid contestId, string? content);
        public Task<ResponseModel> GetRandom(List<ContestDetail> listContestDetail);
        public Task<PagedViewModel> GetByQuestionCategory(int questionCategoryId, int? pageIndex, int? pageSize);
    }
}
