﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.QuestionRepository
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly DapperContext _context;
        public QuestionRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_Question_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.Single();
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<QuestionViewModel> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var questionViewModel = new QuestionViewModel();
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                using (var queryMultiple = await connection.QueryMultipleAsync("SP_Question_GetById", parameters, commandType: CommandType.StoredProcedure))
                {
                    var questionResult = await queryMultiple.ReadAsync<Question>();
                    var question = questionResult.SingleOrDefault();
                    if (question != null)
                    {
                        questionViewModel.Id = question.Id;
                        questionViewModel.ContestId = question.ContestId;
                        questionViewModel.Content = question.Content;
                        questionViewModel.Order = question.Order;
                        questionViewModel.CreatedBy = question.CreatedBy;
                        questionViewModel.CreatedTime = question.CreatedTime;
                        questionViewModel.UpdatedBy = question.UpdatedBy;
                        questionViewModel.UpdatedTime = question.UpdatedTime;

                        var answersResult = await queryMultiple.ReadAsync<Answer>();
                        questionViewModel.Answers = answersResult.ToList();
                    }
                    return questionViewModel;
                }
            }
        }

        public async Task<ResponseModel> Update(QuestionViewModel questionViewModel)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                XElement? answersXML = null;
                try
                {
                    var answers = new List<Answer>();
                    if (questionViewModel.Answers?.Count > 0)
                    {
                        foreach (var answer in questionViewModel.Answers)
                        {
                            answers.Add(answer);
                        }
                    }
                    answersXML = new XElement("Answers",
                        from answer in answers
                        select new XElement("Answer",
                            new XElement("Id", answer.Id),
                            new XElement("ContestId", answer.ContestId),
                            new XElement("QuestionId", answer.QuestionId),
                            new XElement("Content", answer.Content),
                            new XElement("IsCorrect", answer.IsCorrect),
                            new XElement("UpdatedBy", questionViewModel.UpdatedBy)
                        )
                    );

                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", questionViewModel.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Content", questionViewModel.Content, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Order", questionViewModel.Order, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@UpdatedBy", questionViewModel.UpdatedBy, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", questionViewModel.UpdatedTime, DbType.DateTime, ParameterDirection.Input);
                    parameters.Add("@AnswersXML", answersXML?.ToString(), DbType.String, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_Question_Update", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<ContestDetailModel> GetByContestId(Guid contestId, string? questionContent)
        {
            using (var connection = _context.CreateConnection())
            {
                var contestDetailModel = new ContestDetailModel();
                var parameters = new DynamicParameters();
                parameters.Add("@ContestId", contestId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@QuestionContent", questionContent, DbType.String, ParameterDirection.Input);
                parameters.Add("@MaxQuestionOrder", null, DbType.Int32, ParameterDirection.Output);

                using (var queryMultiple = await connection.QueryMultipleAsync("SP_Question_GetByContestId", parameters, commandType: CommandType.StoredProcedure))
                {
                    var contestResult = await queryMultiple.ReadAsync<ContestViewModel>();
                    contestDetailModel.Contest = contestResult.SingleOrDefault();

                    if (contestDetailModel.Contest != null)
                    {
                        var questionsResult = await queryMultiple.ReadAsync<QuestionViewModel>();
                        var questions = questionsResult.ToList();

                        var answersResult = await queryMultiple.ReadAsync<Answer>();
                        var answers = answersResult.ToList();
                        if (questions.Count > 0)
                        {
                            foreach (var question in questions)
                            {
                                question.Answers = answers.Where(x => x.QuestionId == question.Id).ToList();
                            }
                        }
                        contestDetailModel.Questions = questions;
                    }
                    contestDetailModel.MaxQuestionOrder = parameters.Get<int?>("@MaxQuestionOrder") ?? 0;
                }
                return contestDetailModel;
            }
        }

        public async Task<ResponseModel> InsertMany(QuestionsListViewModel questionsListViewModel, Guid createdBy)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    XElement? questionsXML = null;
                    XElement? answersXML = null;
                    if (questionsListViewModel.QuestionViewModels?.Count > 0)
                    {
                        var questionViewModels = questionsListViewModel.QuestionViewModels;
                        var answers = new List<Answer>();
                        foreach (var questionViewModel in questionViewModels)
                        {
                            if (questionViewModel.Answers?.Count > 0)
                            {
                                foreach (var answer in questionViewModel.Answers)
                                {
                                    answers.Add(answer);
                                }
                            }
                        }

                        questionsXML = new XElement("Questions",
                            from questionViewModel in questionViewModels
                            select new XElement("Question",
                                new XElement("Id", questionViewModel.Id),
                                new XElement("ContestId", questionViewModel.ContestId),
                                new XElement("Content", questionViewModel.Content),
                                new XElement("Order", questionViewModel.Order),
                                new XElement("CreatedBy", createdBy)
                            )
                        );

                        answersXML = new XElement("Answers",
                            from answer in answers
                            select new XElement("Answer",
                                new XElement("Id", answer.Id),
                                new XElement("ContestId", answer.ContestId),
                                new XElement("QuestionId", answer.QuestionId),
                                new XElement("Content", answer.Content),
                                new XElement("IsCorrect", answer.IsCorrect),
                                new XElement("CreatedBy", createdBy)
                            )
                        );
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@QuestionsXML", questionsXML?.ToString(), DbType.String, ParameterDirection.Input);
                    parameters.Add("@AnswersXML", answersXML?.ToString(), DbType.String, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_Question_InsertMany", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<List<Question>> Search(Guid? userContestId, Guid contestId, string? content)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserContestId", userContestId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@ContestId", contestId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@Content", content, DbType.String, ParameterDirection.Input);
                var result = await connection.QueryAsync<Question>("SP_Question_Search", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<ResponseModel> GetRandom(List<ContestDetail> listContestDetail)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel() { Status = 1};
                try
                {
                    List<Question> listQuestion = new List<Question>();
                    foreach (var item in listContestDetail)
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@QuestionCategoryId", item.QuestionCategoryId, DbType.Int32, ParameterDirection.Input);
                        parameters.Add("@LevelId", item.LevelId, DbType.Int32, ParameterDirection.Input);
                        parameters.Add("@Quantity", item.Quantity, DbType.Int32, ParameterDirection.Input);

                        using (var queryMultiple = await connection.QueryMultipleAsync("SP_Question_GetRandom", parameters, commandType: CommandType.StoredProcedure))
                        {
                            var questionResult = await queryMultiple.ReadAsync<Question>();
                            listQuestion.AddRange(questionResult.ToList());
                        }
                    }
                    responseModel.Data = listQuestion;
                    return responseModel;
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                    return responseModel;
                }
            }
        }

        public async Task<PagedViewModel> GetByQuestionCategory(int questionCategoryId, int? pageIndex, int? pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();
                try
                {
                    if (!pageIndex.HasValue || pageIndex.Value < 0) pageIndex = 1;
                    if (!pageSize.HasValue || pageSize.Value < 0) pageSize = 24;

                    var parameters = new DynamicParameters();
                    parameters.Add("@QuestionCategoryId", questionCategoryId, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@PageIndex", pageIndex.Value, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@PageSize", pageSize.Value, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Question_GetByQuestionCategoryId", parameters, commandType: CommandType.StoredProcedure))
                    {
                        IEnumerable<QuestionViewModel> questionViewModelsResult = await queryMultiple.ReadAsync<QuestionViewModel>();
                        List<QuestionViewModel> listQuestionViewModelsResult = questionViewModelsResult.ToList();
                        if (listQuestionViewModelsResult.Count > 0)
                        {
                            IEnumerable<Answer> answersResult = await queryMultiple.ReadAsync<Answer>();
                            List<Answer> listAnswer = answersResult.ToList();
                            foreach (var questionViewModel in listQuestionViewModelsResult)
                            {
                                questionViewModel.Answers = listAnswer.Where(x => x.QuestionId == questionViewModel.Id).ToList();
                            }
                        }
                        responseModel.PageIndex = pageIndex.Value;
                        responseModel.PageSize = pageSize.Value;
                        responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                        var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize.Value);
                        responseModel.TotalPage = (int)totalPage;
                        responseModel.Data = listQuestionViewModelsResult;
                    }
                    return responseModel;
                }
                catch (Exception ex)
                {
                    responseModel.Data = null;
                    return responseModel;
                }
            }
        }
    }
}
