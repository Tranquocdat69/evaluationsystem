﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserEvaluationCriteriaRepository
{
    public class UserEvaluationCriteriaRepository : IUserEvaluationCriteriaRepository
    {

        private readonly DapperContext _context;
        public UserEvaluationCriteriaRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid userId, Guid evaluationCriteriaId)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", userId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@EvaluationCriteriaId", evaluationCriteriaId, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_UserEvaluationCriteria_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<List<UserEvaluationCriteria>> GetByUserId(Guid userId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserEvaluationCriteria>("SP_UserEvaluationCriteria_GetByUserId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<ResponseModel> Insert(UserEvaluationCriteria userEvaluationCriteria)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", userEvaluationCriteria.UserId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@EvaluationCriteriaId", userEvaluationCriteria.EvaluationCriteriaId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@CreatedBy", userEvaluationCriteria.CreatedBy, DbType.Guid, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserEvaluationCriteria_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserEvaluationCriteria>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }
    }
}
