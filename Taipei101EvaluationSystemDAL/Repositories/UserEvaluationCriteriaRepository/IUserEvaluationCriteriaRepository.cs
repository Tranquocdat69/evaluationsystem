﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserEvaluationCriteriaRepository
{
    public interface IUserEvaluationCriteriaRepository
    {
        public Task<ResponseModel> Insert(UserEvaluationCriteria userEvaluationCriteria);
        public Task<ResponseModel> Delete(Guid userId, Guid evaluationCriteriaId);
        public Task<List<UserEvaluationCriteria>> GetByUserId(Guid userId);
    }
}
