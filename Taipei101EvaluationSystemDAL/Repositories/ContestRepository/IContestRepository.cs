﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.ContestRepository
{
    public interface IContestRepository
    {
        public Task<PagedViewModel> GetPaging(string? name, int pageIndex, int pageSize);
        public Task<Contest> GetById(Guid id);
        public Task<List<Contest>> GetByContestCategoryId(int contestCategoryId);
        public Task<List<Contest>> GetAll();
        public Task<ResponseModel> Insert(Contest contest);
        public Task<ResponseModel> Update(Contest contest);
        public Task<ResponseModel> Delete(Guid id);
    }
}
