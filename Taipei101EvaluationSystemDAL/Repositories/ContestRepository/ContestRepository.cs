﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.ContestRepository
{
    public class ContestRepository : IContestRepository
    {
        private readonly DapperContext _context;
        public ContestRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_Contest_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<Contest> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<Contest>("SP_Contest_GetById", parameters, commandType: CommandType.StoredProcedure);
                return result.SingleOrDefault();
            }
        }

        public async Task<List<Contest>> GetAll()
        {
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<Contest>("SP_Contest_GetAll", null, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<PagedViewModel> GetPaging(string? name, int pageIndex, int pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();
                var parameters = new DynamicParameters();
                parameters.Add("@Name", name, DbType.String, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                var result = await connection.QueryAsync<ContestViewModel>("SP_Contest_GetPaing", parameters, commandType: CommandType.StoredProcedure);
                var contests = result.ToList();
                responseModel.PageIndex = pageIndex;
                responseModel.PageSize = pageSize;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = contests;
                return responseModel;
            }
        }

        public async Task<ResponseModel> Insert(Contest contest)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Name", contest.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Time", contest.Time, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@Level", contest.Level, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@QuestionNumber", contest.QuestionNumber, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@CreatedBy", contest.CreatedBy, DbType.Guid, ParameterDirection.Input);


                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Contest_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<Contest>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }

        public async Task<ResponseModel> Update(Contest contest)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", contest.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@Name", contest.Name, DbType.String, ParameterDirection.Input);
                    parameters.Add("@Time", contest.Time, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@Level", contest.Level, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@QuestionNumber", contest.QuestionNumber, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@UpdatedBy", contest.UpdatedBy, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", contest.UpdatedTime, DbType.DateTime, ParameterDirection.Input);
                    parameters.Add("@Status", contest.Status, DbType.Boolean, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_Contest_Update", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<Contest>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }

        public async Task<List<Contest>> GetByContestCategoryId(int contestCategoryId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContestCategoryId", contestCategoryId, DbType.Int32, ParameterDirection.Input);
                var result = await connection.QueryAsync<Contest>("SP_Contest_GetByContestCategoryId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }
    }
}
