﻿using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories
{
    public interface IContestCategoryRepository
    {
        Task<PagedViewModel> GetPaging(string? searchingText, int? pageIndex, int? pageSize);
    }
}
