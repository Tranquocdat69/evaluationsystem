﻿using Dapper;
using System.Data;
using Taipei101EvaluationSystemModels;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;

namespace Taipei101EvaluationSystemDAL.Repositories
{
    public class ContestCategoryRepository : IContestCategoryRepository
    {
        private readonly DapperContext _context;
        public ContestCategoryRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<PagedViewModel> GetPaging(string? searchingText, int? pageIndex, int? pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();

                if (!pageIndex.HasValue) pageIndex = 1;
                if (!pageSize.HasValue) pageSize = 24;

                var parameters = new DynamicParameters();
                parameters.Add("@Name", searchingText, DbType.String, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                IEnumerable<ContestCategoryModel> result = await connection.QueryAsync<ContestCategoryModel>("SP_ContestCategory_GetPaging", parameters, commandType: CommandType.StoredProcedure);
                List<ContestCategoryModel> listContestCategoryAll = result.ToList();
                List<ContestCategoryModel> listContestCategory = listContestCategoryAll.Where(x => x.ParentId == 0).OrderBy(x => x.Order).ToList();

                RecursiveGetListChild(listContestCategoryAll, listContestCategory);

                responseModel.PageIndex = pageIndex.Value;
                responseModel.PageSize = pageSize.Value;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize.Value);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = listContestCategory;
                return responseModel;
            }
        }

        private void RecursiveGetListChild(List<ContestCategoryModel> listContestCategoryAll, List<ContestCategoryModel> listContestCategory)
        {
            foreach (var item in listContestCategory)
            {
                item.ListChild = listContestCategoryAll.Where(x => x.ParentId == item.Id).OrderBy(x => x.Order).ToList();
                if (item.ListChild.Count > 0)
                    RecursiveGetListChild(listContestCategoryAll, item.ListChild);
            }
        }
    }
}
