﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Repositories.ActionRepository
{
    public class ActionRepository : IActionRepository
    {
        private readonly DapperContext _context;
        public ActionRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<List<Entities.Action>> GetAll(string? sort)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                //parameters.Add("@Sort", sort, DbType.String, ParameterDirection.Input);
                var result = await connection.QueryAsync<Entities.Action>("SP_Action_GetAll", commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public List<Entities.Action> GetByRoleId(Guid roleId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@RoleId", roleId, DbType.Guid, ParameterDirection.Input);
                var result = connection.Query<Entities.Action>("SP_Action_GetByRoleId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<List<Entities.Action>> GetByRoleIdAsync(Guid roleId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@RoleId", roleId, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<Entities.Action>("SP_Action_GetByRoleId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }
    }
}
