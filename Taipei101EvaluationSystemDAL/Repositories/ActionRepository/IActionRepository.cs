﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Repositories.ActionRepository
{
    public interface IActionRepository
    {
        public List<Entities.Action> GetByRoleId(Guid roleId);
        public Task<List<Entities.Action>> GetByRoleIdAsync(Guid roleId);
        public Task<List<Entities.Action>> GetAll(string? sort);
    }
}
