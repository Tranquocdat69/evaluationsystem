﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository
{
    public interface IUserContestAnswerRepository
    {
        public Task<ResponseModel> Insert(UserContestAnswer userContestAnswer);
        public Task<ResponseModel> Delete(UserContestAnswer userContestAnswer);
        public Task<List<UserContestAnswer>> GetByUserContestId(Guid userContestId);
    }
}
