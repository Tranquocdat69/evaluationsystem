﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository
{
    public class UserContestAnswerRepository : IUserContestAnswerRepository
    {
        private readonly DapperContext _context;
        public UserContestAnswerRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(UserContestAnswer @UserContestId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserContestId", @UserContestId.UserContestId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@QuestionId", @UserContestId.QuestionId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@AnswerId", @UserContestId.AnswerId, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<ResponseModel>("SP_UserContestAnswer_Delete", parameters, commandType: CommandType.StoredProcedure);
                return result.SingleOrDefault();
            }
        }

        public async Task<List<UserContestAnswer>> GetByUserContestId(Guid userContestId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserContestId", userContestId, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserContestAnswer>("SP_UserContestAnswer_GetByUserContestId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<ResponseModel> Insert(UserContestAnswer userContestAnswer)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserContestId", userContestAnswer.UserContestId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@QuestionId", userContestAnswer.QuestionId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@AnswerId", userContestAnswer.AnswerId, DbType.Guid, ParameterDirection.Input);


                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserContestAnswer_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserContestAnswer>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }
    }
}
