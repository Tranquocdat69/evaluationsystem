﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserContestRepository
{
    public class UserContestRepository : IUserContestRepository
    {
        private readonly DapperContext _context;
        public UserContestRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel> Delete(Guid id)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                    var result = await connection.QueryAsync<ResponseModel>("SP_UserContest_Delete", parameters, commandType: CommandType.StoredProcedure);
                    responseModel = result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                responseModel.Data = null;
            }
            return responseModel;
        }

        public async Task<UserContest> GetById(Guid id)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserContest>("SP_UserContest_GetById", parameters, commandType: CommandType.StoredProcedure);
                return result.Single();
            }
        }

        public async Task<PagedViewModel> GetPaging(Guid? userId, string? contestName, int? type, int pageIndex, int pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@ContestName", contestName, DbType.String, ParameterDirection.Input);
                parameters.Add("@Type", type, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                var result = await connection.QueryAsync<UserContestViewModel>("SP_UserContest_GetPaging", parameters, commandType: CommandType.StoredProcedure);
                var userContests = result.ToList();
                responseModel.PageIndex = pageIndex;
                responseModel.PageSize = pageSize;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = userContests;
                return responseModel;
            }
        }

        public async Task<ResponseModel> Insert(UserContest userContest)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", userContest.UserId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@ContestId", userContest.ContestId, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@CreatedBy", userContest.CreatedBy, DbType.Guid, ParameterDirection.Input);


                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserContest_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status != 0)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserContest>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }

        public async Task<ResponseModel> Update(UserContest userContest)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Id", userContest.Id, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@CorrectAnswer", userContest.CorrectAnswer, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@WrongAnswer", userContest.WrongAnswer, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@NotAnswer", userContest.NotAnswer, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@StartedTime", userContest.StartedTime, DbType.DateTime, ParameterDirection.Input);
                    parameters.Add("@EndedTime", userContest.EndedTime, DbType.DateTime, ParameterDirection.Input);
                    parameters.Add("@UpdatedBy", userContest.UpdatedBy, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@UpdatedTime", userContest.UpdatedTime, DbType.DateTime, ParameterDirection.Input);

                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserContest_Update", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status != -99)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserContestViewModel>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }
                return responseModel;
            }
        }
    }
}
