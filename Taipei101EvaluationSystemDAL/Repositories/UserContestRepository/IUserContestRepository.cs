﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserContestRepository
{
    public interface IUserContestRepository
    {
        public Task<ResponseModel> Insert(UserContest userContest);
        public Task<ResponseModel> Delete(Guid id);
        public Task<UserContest> GetById(Guid id);
        public Task<PagedViewModel> GetPaging(Guid? userId, string? contestName, int? type, int pageIndex, int pageSize);
        public Task<ResponseModel> Update(UserContest userContest);
    }
}
