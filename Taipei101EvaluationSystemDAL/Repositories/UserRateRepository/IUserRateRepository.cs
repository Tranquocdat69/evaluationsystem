﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserRateRepository
{
    public interface IUserRateRepository
    {
        public Task<ResponseModel> Insert(UserRate userRate);
        public Task<List<UserRate>> GetAll();
        public Task<List<UserRateViewModel>> GetByRateFor(Guid userId);
        public Task<UserRateViewModel> GetDetail(Guid rateBy, Guid rateFor);
        public Task<List<UserRateViewModel>> GetAll(Guid departmentId, Guid userId);
    }
}
