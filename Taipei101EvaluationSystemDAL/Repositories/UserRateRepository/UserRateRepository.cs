﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.UserRateRepository
{
    public class UserRateRepository : IUserRateRepository
    {
        private readonly DapperContext _context;
        public UserRateRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<List<UserRate>> GetAll()
        {
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<UserRate>("SP_UserRate_GetAll", commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<List<UserRateViewModel>> GetAll(Guid departmentId, Guid userId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@DepartmentId", departmentId, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@UserId", userId, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserRateViewModel>("SP_UserRate_GetAllByDepartmentIdAndUserId", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<List<UserRateViewModel>> GetByRateFor(Guid userId)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserRateViewModel>("SP_UserRate_GetByRateFor", parameters, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<UserRateViewModel> GetDetail(Guid rateBy, Guid rateFor)
        {
            using (var connection = _context.CreateConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@RateBy", rateBy, DbType.Guid, ParameterDirection.Input);
                parameters.Add("@RateFor", rateFor, DbType.Guid, ParameterDirection.Input);
                var result = await connection.QueryAsync<UserRateViewModel>("SP_UserRate_GetDetail", parameters, commandType: CommandType.StoredProcedure);
                return result.Single();
            }
        }

        public async Task<ResponseModel> Insert(UserRate userRate)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new ResponseModel();
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@RateBy", userRate.RateBy, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@RateFor", userRate.RateFor, DbType.Guid, ParameterDirection.Input);
                    parameters.Add("@StarNumber", userRate.StarNumber, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@RatedContent", userRate.RatedContent, DbType.String, ParameterDirection.Input);


                    using (var queryMultiple = await connection.QueryMultipleAsync("SP_UserRate_Insert", parameters, commandType: CommandType.StoredProcedure))
                    {
                        var reponseModelResult = await queryMultiple.ReadAsync<ResponseModel>();
                        responseModel = reponseModelResult.SingleOrDefault();
                        if (responseModel.Status == 1)
                        {
                            var userViewModelResult = await queryMultiple.ReadAsync<UserRate>();
                            responseModel.Data = userViewModelResult.SingleOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseModel.Status = -99;
                    responseModel.Message = ex.Message;
                    responseModel.Data = null;
                }

                return responseModel;
            }
        }
    }
}
