﻿using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.QuestionCategoryRepository
{
    public interface IQuestionCategoryRepository
    {
        public Task<PagedViewModel> GetPaging(string? searchingText, int? pageIndex, int? pageSize);
    }
}
