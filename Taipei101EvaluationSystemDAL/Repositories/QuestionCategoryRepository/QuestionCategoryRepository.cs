﻿using Dapper;
using System.Data;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.Repositories.QuestionCategoryRepository
{
    public class QuestionCategoryRepository : IQuestionCategoryRepository
    {
        private readonly DapperContext _context;

        public QuestionCategoryRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<PagedViewModel> GetPaging(string? searchingText, int? pageIndex, int? pageSize)
        {
            using (var connection = _context.CreateConnection())
            {
                var responseModel = new PagedViewModel();

                if (!pageIndex.HasValue) pageIndex = 1;
                if (!pageSize.HasValue) pageSize = 24;

                var parameters = new DynamicParameters();
                parameters.Add("@Name", searchingText, DbType.String, ParameterDirection.Input);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@PageSize", pageSize, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@TotalRecord", null, DbType.Int32, ParameterDirection.Output);
                IEnumerable<QuestionCategoryModel> result = await connection.QueryAsync<QuestionCategoryModel>("SP_QuestionCategory_GetPaging", parameters, commandType: CommandType.StoredProcedure);
                List<QuestionCategoryModel> listContestCategoryAll = result.ToList();
                List<QuestionCategoryModel> listContestCategory = listContestCategoryAll.Where(x => x.ParentId == 0).ToList();

                RecursiveGetListChild(listContestCategoryAll, listContestCategory);

                responseModel.PageIndex = pageIndex.Value;
                responseModel.PageSize = pageSize.Value;
                responseModel.TotalRecord = parameters.Get<int>("@TotalRecord");
                var totalPage = Math.Ceiling(Convert.ToDouble(responseModel.TotalRecord) / pageSize.Value);
                responseModel.TotalPage = (int)totalPage;
                responseModel.Data = listContestCategory;
                return responseModel;
            }
        }

        private void RecursiveGetListChild(List<QuestionCategoryModel> listContestCategoryAll, List<QuestionCategoryModel> listContestCategory)
        {
            foreach (var item in listContestCategory)
            {
                item.ListChild = listContestCategoryAll.Where(x => x.ParentId == item.Id).ToList();
                if (item.ListChild.Count > 0)
                    RecursiveGetListChild(listContestCategoryAll, item.ListChild);
            }
        }
    }
}
