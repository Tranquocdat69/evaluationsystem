﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class UserViewModel : User
    {
        public string? DepartmentName { get; set; }
        public string? RoleName { get; set; }
    }
}
