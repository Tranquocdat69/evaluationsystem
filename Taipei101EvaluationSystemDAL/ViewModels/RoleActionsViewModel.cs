﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class RoleActionsViewModel
    {
        public Guid? RoleId { get; set; }
        public string? ActionIds { get; set; }
    }
}
