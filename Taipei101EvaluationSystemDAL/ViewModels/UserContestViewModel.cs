﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class UserContestViewModel : UserContest
    {
        public string? UserContestName { get; set; }
        public string? ContestName { get; set; }
        public int? ContestTime { get; set; }
        public int? ContestLevel { get; set; }
        public int? ContestQuestionNumber { get; set; }
        public string? CreatedByName { get; set; }
    }

    public class UserContestUpsertRequest
    {
        public Guid UserId { get; set; }
        public Guid ContestId { get; set; }
    }

    public class AggregatedUserContest
    {
        public List<UserContestAnswer> ListChosenAnswer { get; set; }
        public UserContestHasNoIsCorrect UserContestQuestion { get; set; }
    }
}
