﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class ContestDetailModel
    {
        public int MaxQuestionOrder { get; set; }
        public ContestViewModel? Contest { get; set; }
        public List<QuestionViewModel>? Questions { get; set; }
    }
}
