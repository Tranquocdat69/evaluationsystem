﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class UserContestQuestionsViewModel
    {
        public List<UserContestQuestion>? UserContestQuestions { get; set; }
    }
}
