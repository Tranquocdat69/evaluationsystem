﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class DepartmentViewModel : Department
    {
        public string? CreatedByName { get; set; }
        public string? UpdatedByName { get; set; }
    }
}
