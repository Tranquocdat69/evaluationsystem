﻿using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class RoleViewModel : Role
    {
        public string NameOfCreatedByUser { get; set; }
        public string NameOfUpdatedByUser { get; set; }
    }
}