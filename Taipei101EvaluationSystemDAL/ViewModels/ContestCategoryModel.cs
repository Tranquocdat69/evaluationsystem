﻿using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class ContestCategoryModel : ContestCategory
    {
        public int TotalContest { get; set; }
        public List<ContestCategoryModel> ListChild { get; set; } = new List<ContestCategoryModel>();
    }
}
