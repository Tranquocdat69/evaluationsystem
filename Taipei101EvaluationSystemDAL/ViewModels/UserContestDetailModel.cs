﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class UserContestDetailModel
    {
        public UserContestViewModel? UserContest { get; set; }
        public List<QuestionViewModel>? Questions { get; set; }
    }
}
