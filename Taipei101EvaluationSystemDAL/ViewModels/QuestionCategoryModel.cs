﻿using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class QuestionCategoryModel : QuestionCategory
    {
        public List<QuestionCategoryModel> ListChild { get; set; } = new List<QuestionCategoryModel>();
    }
}
