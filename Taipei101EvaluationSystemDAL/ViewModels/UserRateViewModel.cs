﻿using Taipei101EvaluationSystemDAL.Entities;

namespace Taipei101EvaluationSystemDAL.ViewModels
{
    public class UserRateViewModel : UserRate
    {
        public string? RateByName { get; set; }
        public string? RateByAvatar { get; set; }
    }
}
