﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class UserContestQuestion
    {
        public Guid? UserContestId { get; set; }
        public Guid? QuestionId { get; set; }
    }
}
