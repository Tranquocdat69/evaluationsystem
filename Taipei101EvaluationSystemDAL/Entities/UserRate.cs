﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class UserRate
    {
        public Guid? RateBy { get; set; }
        public Guid? RateFor { get; set; }
        public int? StarNumber { get; set; }
        public string? RatedContent { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
