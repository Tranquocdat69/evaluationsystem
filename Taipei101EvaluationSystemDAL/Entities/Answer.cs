﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class Answer
    {
        public Guid Id { get; set; }
        public Guid? ContestId { get; set; }
        public Guid? QuestionId { get; set; }
        public string? Content { get; set; }
        public bool? IsCorrect { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}
