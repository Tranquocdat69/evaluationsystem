﻿namespace Taipei101EvaluationSystemDAL.Entities
{
    public class ContestCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public int ParentId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTime { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}
