﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class UserEvaluationCriteria
    {
        public Guid? UserId { get; set; }
        public Guid? EvaluationCriteriaId { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
