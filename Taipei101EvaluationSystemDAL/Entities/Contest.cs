﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class Contest
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public int? Time { get; set; }
        public int? Level { get; set; }
        public int? QuestionNumber { get; set; }
        public int ContestCategoryId { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? Status { get; set; }
    }
}
