﻿namespace Taipei101EvaluationSystemDAL.Entities
{
    public class ContestDetail
    {
        public Guid ContestId { get; set; }
        public int QuestionCategoryId { get; set; }
        public int LevelId { get; set; }
        public int Quantity { get; set; }
    }
}
