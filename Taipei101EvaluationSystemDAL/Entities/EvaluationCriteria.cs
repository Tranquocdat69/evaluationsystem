﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class EvaluationCriteria
    {
        public Guid Id { get; set; }
        public string? Criteria { get; set; }
        public int? Level { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? Status { get; set; }
        public int? Order { get; set; }
    }

}
