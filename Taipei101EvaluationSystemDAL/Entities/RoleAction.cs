﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taipei101EvaluationSystemDAL.Entities
{
    public class RoleAction
    {
        public Guid? RoleId { get; set; }
        public Guid? ActionId { get; set; }
    }
}
