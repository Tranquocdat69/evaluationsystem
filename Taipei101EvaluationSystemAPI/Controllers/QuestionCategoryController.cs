﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Repositories.QuestionCategoryRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class QuestionCategoryController : ControllerBase
    {
        private readonly IQuestionCategoryRepository _questionCategoryRepository;

        public QuestionCategoryController(IQuestionCategoryRepository questionCategoryRepository)
        {
            _questionCategoryRepository = questionCategoryRepository;
        }

        [HttpGet]
        [Route("questionCategories")]
        [Role(Constants.CLAIM_TYPE, "GetAllQuestions")]
        public async Task<IActionResult> GetPaging([FromQuery] string? searchingText, int? pageIndex, int? pageSize)
        {
            PagedViewModel pagedViewModel = await _questionCategoryRepository.GetPaging(searchingText, pageIndex, pageSize);
            return Ok(pagedViewModel);
        }
    }
}
