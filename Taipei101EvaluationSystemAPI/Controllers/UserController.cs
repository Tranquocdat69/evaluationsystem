﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepositories;
        Utilities? utilities;

        public UserController(IUserRepository userRepository, IConfiguration configuration)
        {
            _userRepositories = userRepository;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(User user)
        {
            utilities = new Utilities();
            var jwtAuthenticationManager = new JwtAuthenticationManager(_userRepositories, _configuration);

            user.Password = utilities.CreateMD5(user.Password);
            var responseModel = await jwtAuthenticationManager.Authenticate(user);

            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "InsertUser")]
        [HttpPost]
        [Route("Insert")]
        public async Task<IActionResult> Insert(User user)
        {
            utilities = new Utilities();
            user.CreatedTime = DateTime.Now;
            user.Password = utilities.CreateMD5(user.Password);
            var response = await _userRepositories.Insert(user);
            return Ok(response);
        }

        [Role(Constants.CLAIM_TYPE, "UpdateUser")]
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(User user)
        {
            utilities = new Utilities();
            user.UpdatedTime = DateTime.Now;
            if (!string.IsNullOrEmpty(user.Password))
            {
                user.Password = utilities.CreateMD5(user.Password);
            }

            var response = await _userRepositories.Update(user);
            return Ok(response);
        }
        
        [Role(Constants.CLAIM_TYPE, "UpdateUser")]
        [HttpPut]
        [Route("UpdateProfile")]
        public async Task<IActionResult> UpdateProfile(User user)
        {
            user.UpdatedTime = DateTime.Now;
            var response = await _userRepositories.UpdateProfile(user);
            return Ok(response);
        }

        [Role(Constants.CLAIM_TYPE, "GetPagingUsers")]
        [HttpGet]
        [Route("GetPaging")]
        public async Task<IActionResult> GetPaging(string? searchingText,  Guid? departmentId, int pageIndex = 1, int pageSize = 10)
        {
            var users = await _userRepositories.GetPaging(searchingText, departmentId, pageIndex, pageSize);
            return Ok(users);
        }

        [Role(Constants.CLAIM_TYPE, "GetUserById")]
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var user = await _userRepositories.GetById(id);
            return Ok(user);
        }

        [Role(Constants.CLAIM_TYPE, "GetUserById")]
        [HttpGet]
        [Route("GetByUserName")]
        public async Task<IActionResult> GetDetailByUsername(string userName)
        {
            var user = await _userRepositories.GetDetailByUsername(userName);
            return Ok(user);
        }

        [Role(Constants.CLAIM_TYPE, "GetPagingUsers")]
        [HttpGet]
        [Route("GetUsersToRatePaging")]
        public async Task<IActionResult> GetUsersToRatePaging(Guid departmentId, string username, int pageIndex = 1, int pageSize = 12)
        {
            var user = await _userRepositories.GetUsersToRatePaging(departmentId, username, pageIndex, pageSize);
            return Ok(user);
        }

        [Role(Constants.CLAIM_TYPE, "UpdateUser")]
        [HttpPut]
        [Route("UpdatePassword")]
        public async Task<IActionResult> UpdatePassword(User user)
        {
            utilities = new Utilities();
            user.UpdatedTime = DateTime.Now;
            if (!string.IsNullOrEmpty(user.Password))
            {
                user.Password = utilities.CreateMD5(user.Password);
            }
            var responseModel = await _userRepositories.UpdatePassword(user);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "DeleteUser")]
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var responseModel = await _userRepositories.Delete(id);
            return Ok(responseModel);
        }
    }
}
