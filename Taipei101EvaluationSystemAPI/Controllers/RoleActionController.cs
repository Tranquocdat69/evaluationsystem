﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.RoleActionRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleActionController : ControllerBase
    {
        private readonly IRoleActionRepository _roleActionRepository;

        public RoleActionController(
            IRoleActionRepository roleActionRepository
        )
        {
            _roleActionRepository = roleActionRepository;
        }

        [Role(Constants.CLAIM_TYPE, "InsertRoleAction")]
        [HttpPost]
        [Route("Insert")]
        public async Task<IActionResult> Insert(RoleAction roleAction)
        {
            var responseModel = await _roleActionRepository.Insert(roleAction);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "InsertRoleActions")]
        [HttpPost]
        [Route("InsertMany")]
        public async Task<IActionResult> InsertMany(RoleActionsViewModel roleActionsViewModel)
        {
            var responseModel = await _roleActionRepository.InsertMany(roleActionsViewModel);
            return Ok(responseModel);
        }
    }
}
