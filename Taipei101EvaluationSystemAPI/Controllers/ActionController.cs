﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Repositories.ActionRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionController : ControllerBase
    {
        private readonly IActionRepository _actionRepository;

        public ActionController(IActionRepository actionRepository)
        {
            _actionRepository = actionRepository;
        }

        [Role(Constants.CLAIM_TYPE, "GetActionsByRoleId")]
        [HttpGet]
        [Route("GetByRoleId")]
        public async Task<IActionResult> GetByRoleId(Guid roleId)
        {
            var responseModel = await _actionRepository.GetByRoleIdAsync(roleId);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "GetAllActions")]
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll(string? sort)
        {
            var responseModel = await _actionRepository.GetAll(sort);
            return Ok(responseModel);
        }
    }
}
