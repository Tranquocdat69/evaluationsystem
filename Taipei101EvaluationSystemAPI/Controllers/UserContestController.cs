﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class UserContestController : ControllerBase
    {
        private readonly IUserContestRepository _userContestRepository;
        private readonly IContestDetailRepository _contestDetailRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IUserContestQuestionRepository _userContestQuestionRepository;
        private readonly IUserContestAnswerRepository _userContestAnswerRepository;

        public UserContestController(IUserContestRepository userContestRepository, IContestDetailRepository contestDetailRepository, IQuestionRepository questionRepository, IUserContestQuestionRepository userContestQuestionRepository, IUserContestAnswerRepository userContestAnswerRepository)
        {
            _userContestRepository = userContestRepository;
            _contestDetailRepository = contestDetailRepository;
            _questionRepository = questionRepository;
            _userContestQuestionRepository = userContestQuestionRepository;
            _userContestAnswerRepository = userContestAnswerRepository;
        }

        [HttpGet]
        [Route("userContests/{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var responseModel = await _userContestRepository.GetById(id);
            return Ok(responseModel);
        }

        [HttpPost]
        [Route("userContests")]
        [Role(Constants.CLAIM_TYPE, "StartContest")]
        public async Task<IActionResult> GetOrInsert([FromBody] UserContestUpsertRequest request)
        {
            UserContest userContest = new UserContest() { UserId = request.UserId, ContestId = request.ContestId };
            var responseModel = await _userContestRepository.Insert(userContest);
            ResponseModel result = new() { Status = responseModel.Status, Message = responseModel.Message };
            AggregatedUserContest aggregatedUserContest = new AggregatedUserContest();
            if (responseModel.Status != 0)
            {
                var userContestResponse = (UserContest)responseModel.Data;
                if (responseModel.Status == 1)
                {
                    List<ContestDetail> listContestDetail = await _contestDetailRepository.GetByContestId(request.ContestId);
                    var listRandomQuestion = await _questionRepository.GetRandom(listContestDetail);
                    List<Question>? listQuestion = (List<Question>) listRandomQuestion.Data;

                    List<UserContestQuestion> userContestQuestions = new List<UserContestQuestion>();
                    listQuestion.ForEach(x => userContestQuestions.Add(new UserContestQuestion() { UserContestId = userContestResponse.Id, QuestionId = x.Id }));
                    if (userContestQuestions.Count > 0)
                    {
                        UserContestQuestionsViewModel userContestQuestionsViewModel = new UserContestQuestionsViewModel() { UserContestQuestions = userContestQuestions };
                        await _userContestQuestionRepository.InsertMany(userContestQuestionsViewModel);
                    }
                }

                List<UserContestAnswer> listChosenAnswer = await _userContestAnswerRepository.GetByUserContestId(userContestResponse.Id);
                UserContestHasNoIsCorrect userContestQuestion = await _userContestQuestionRepository.GetAllHasNoIsCorrect(userContestResponse.Id, request.UserId);

                aggregatedUserContest = new AggregatedUserContest() { ListChosenAnswer = listChosenAnswer, UserContestQuestion = userContestQuestion };
                result.Data = aggregatedUserContest;
            }
            return Ok(result);
        }
    }
}
