﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Repositories;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class ContestCategoryController : ControllerBase
    {
        private readonly IContestCategoryRepository _contestCategoryRepository;

        public ContestCategoryController(IContestCategoryRepository contestCategoryRepository)
        {
            _contestCategoryRepository = contestCategoryRepository;
        }

        [HttpGet]
        [Route("contestCategories")]
        [Role(Constants.CLAIM_TYPE, "GetPagingContests")]
        public async Task<IActionResult> GetPaging([FromQuery] string? searchingText, int? pageIndex, int? pageSize)
        {
            PagedViewModel pagedViewModel = await _contestCategoryRepository.GetPaging(searchingText, pageIndex, pageSize);
            return Ok(pagedViewModel);
        }
    }
}
