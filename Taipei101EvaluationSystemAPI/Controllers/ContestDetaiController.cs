﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class ContestDetaiController : ControllerBase
    {
        private readonly IContestDetailRepository _contestDetailRepository;

        public ContestDetaiController(IContestDetailRepository contestDetailRepository)
        {
            _contestDetailRepository = contestDetailRepository;
        }

        [HttpGet]
        [Route("contests/{contestId}/contestDetails")]
        public async Task<IActionResult> GetByContestId(Guid contestId)
        {
            List<Taipei101EvaluationSystemDAL.Entities.ContestDetail> contestDetails = await _contestDetailRepository.GetByContestId(contestId);
            return Ok(contestDetails);
        }
    }
}
