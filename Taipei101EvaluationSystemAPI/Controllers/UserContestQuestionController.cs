﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.ViewModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class UserContestQuestionController : ControllerBase
    {
        private readonly IUserContestQuestionRepository _userContestQuestionRepository;

        public UserContestQuestionController(IUserContestQuestionRepository userContestQuestionRepository)
        {
            _userContestQuestionRepository = userContestQuestionRepository;
        }

        [HttpPost]
        [Route("userContestQuestions")]
        public async Task<IActionResult> InsertMany([FromBody] UserContestQuestionsViewModel userContestQuestionsViewModel)
        {
            Taipei101EvaluationSystemModels.ResponseModel responseModel = await _userContestQuestionRepository.InsertMany(userContestQuestionsViewModel);
            return Ok(responseModel);
        }
    }
}
