﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserRateRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRateController : ControllerBase
    {
        private readonly IUserRateRepository _userRateRepository;
        public UserRateController(IUserRateRepository userRateRepository)
        {
            _userRateRepository = userRateRepository;
        }

        [Role(Constants.CLAIM_TYPE, "InsertUserRate")]
        [HttpPost]
        [Route("Insert")]
        public async Task<IActionResult> Insert(UserRate userRate)
        {
            var response = await _userRateRepository.Insert(userRate);
            return Ok(response);
        }

        [Role(Constants.CLAIM_TYPE, "InsertUserRate")]
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll(Guid departmentId, Guid userId)
        {
            var response = await _userRateRepository.GetAll(departmentId, userId);
            return Ok(response);
        }
        
        [Role(Constants.CLAIM_TYPE, "InsertUserRate")]
        [HttpGet]
        [Route("GetDetail")]
        public async Task<IActionResult> GetDetail(Guid rateBy, Guid rateFor)
        {
            var response = await _userRateRepository.GetDetail(rateBy, rateFor);
            return Ok(response);
        }
    }
}
