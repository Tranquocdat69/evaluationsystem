﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleRepository _roleRepository;

        public RoleController(
            IRoleRepository roleRepository
        )
        {
            _roleRepository = roleRepository;
        }

        [Role(Constants.CLAIM_TYPE, "GetRoleById")]
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var responseModel = await _roleRepository.GetById(id);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "GetAllRoles")]
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll(string sort)
        {
            var responseModel = await _roleRepository.GetAll(sort);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "InsertRole")]
        [HttpPost]
        [Route("Insert")]
        public async Task<IActionResult> Insert(Role role)
        {
            var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(User.FindFirst(Constants.CLAIM_TYPE).Value);
            role.CreatedTime = DateTime.Now;
            role.CreatedBy = logedInUser?.Id;
            var responseModel = await _roleRepository.Insert(role);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "UpdateRole")]
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(Role role)
        {
            var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(User.FindFirst(Constants.CLAIM_TYPE).Value);
            role.UpdatedTime = DateTime.Now;
            role.UpdatedBy = logedInUser?.Id;
            var responseModel = await _roleRepository.Update(role);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "DeleteRole")]
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var responseModel = await _roleRepository.Delete(id);
            return Ok(responseModel);
        }
    }
}
