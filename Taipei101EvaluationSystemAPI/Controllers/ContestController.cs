﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.ContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContestController : ControllerBase
    {
        private readonly IUserContestRepository _userContestRepository;
        private readonly IUserContestQuestionRepository _userContestQuestionRepository;
        private readonly IUserContestAnswerRepository _userContestAnswerRepository;
        private readonly IContestRepository _contestRepository;

        public ContestController(
            IUserContestRepository userContestRepository, 
            IUserContestQuestionRepository userContestQuestionRepository, 
            IUserContestAnswerRepository userContestAnswerRepository, 
            IContestRepository contestRepository)
        {
            _userContestRepository = userContestRepository;
            _userContestQuestionRepository = userContestQuestionRepository;
            _userContestAnswerRepository= userContestAnswerRepository;
            _contestRepository = contestRepository;
        }

        [HttpGet]
        [Route("GetPaging")]
        [Role(Constants.CLAIM_TYPE, "GetPagingContests")]
        public async Task<IActionResult> GetPaging(string? contestName, int? type, int pageIndex = 1, int pageSize = 12)
        {
            var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(User?.FindFirst(Constants.CLAIM_TYPE).Value);
            var responseModel = await _userContestRepository.GetPaging(logedInUser?.Id, contestName, type, pageIndex, pageSize);
            return Ok(responseModel);
        }

        [HttpGet]
        [Route("contestCategories/{contestCategoryId}/contests")]
        [Role(Constants.CLAIM_TYPE, "GetPagingContests")]
        public async Task<IActionResult> GetByContestCategoryId(int contestCategoryId)
        {
            var responseModel = await _contestRepository.GetByContestCategoryId(contestCategoryId);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "StartContest")]
        [HttpGet]
        [Route("Start")]
        public async Task<IActionResult> Start(Guid id)
        {
            var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(User.FindFirst(Constants.CLAIM_TYPE).Value);
            var userContest = new UserContest();
            userContest.Id = id;
            userContest.StartedTime = DateTime.Now;
            userContest.UpdatedTime = DateTime.Now;
            userContest.UpdatedBy = logedInUser?.Id;
            var responseModel = await _userContestRepository.Update(userContest);
            return Ok(responseModel);
        }

        [Role(Constants.CLAIM_TYPE, "EndContest")]
        [HttpGet]
        [Route("End")]
        public async Task<IActionResult> End(Guid id)
        {
            //UserContest checkUserContest = await _userContestRepository.GetById(id);
            //if (checkUserContest.EndedTime is not null)
            //    return Ok(new ResponseModel() { Status = -99, Message = "Bài thi đã được hoàn thành không thể nộp bài thêm lần nữa!" });

            PostExam postExam = new();

            int correctAnswersNumber = 0;
            int wrongAnswersNumber = 0;
            int notAnswersNumber = 0;

            var userContestAnswers = await _userContestAnswerRepository.GetByUserContestId(id);
            var userContestQuestions = await _userContestQuestionRepository.GetByUserContestId(id, null);
            if (userContestQuestions.Questions?.Count > 0)
            {
                foreach (var question in userContestQuestions.Questions)
                {
                    var questionAnswers = userContestAnswers.Where(x => x.QuestionId == question.Id).ToList();
                    if (!questionAnswers.Any())
                    {
                        PostExamQuestionInfo postExamQuestionInfo = new PostExamQuestionInfo() { 
                            Id = question.Id, 
                            Content = question.Content, 
                            Explanation = question.Explanation,
                            Type = PostExamQuestionType.NotAnswer,
                            ListAnswer = new()
                        };
                        foreach (var item in question.Answers)
                        {
                            postExamQuestionInfo.ListAnswer.Add(new PostExamAnswerInfo()
                            {
                                Id = item.Id,
                                QuestionId = item.QuestionId,
                                Content = item.Content,
                                IsCorrect = item.IsCorrect,
                                IsChosen = false
                            });
                        };
                        postExam.ListQuestion.Add(postExamQuestionInfo);

                        notAnswersNumber++;
                        continue;
                    }
                    var correctAnswers = question.Answers?.Where(x => x.IsCorrect == true)?.ToList();
                    if (correctAnswers?.Count > 0 && questionAnswers.Count <= correctAnswers.Count)
                    {
                        int checkCorrect = 0;
                        foreach (var correctAnswer in correctAnswers)
                        {
                            if (questionAnswers.Find(x => x.AnswerId == correctAnswer.Id) is not null)
                                checkCorrect++;
                        }
                        if (checkCorrect == correctAnswers.Count)
                        {
                            PostExamQuestionInfo postExamQuestionInfo = new PostExamQuestionInfo()
                            {
                                Id = question.Id,
                                Content = question.Content,
                                Explanation = question.Explanation,
                                Type = PostExamQuestionType.CorrectAnswer,
                                ListAnswer = new()
                            };
                            foreach (var item in question.Answers)
                            {
                                postExamQuestionInfo.ListAnswer.Add(new PostExamAnswerInfo()
                                {
                                    Id = item.Id,
                                    QuestionId = item.QuestionId,
                                    Content = item.Content,
                                    IsCorrect = item.IsCorrect,
                                    IsChosen = questionAnswers.Any(x => x.AnswerId == item.Id)
                                });
                            };
                            postExam.ListQuestion.Add(postExamQuestionInfo);
                            correctAnswersNumber++;
                        }
                        else
                        {
                            PostExamQuestionInfo postExamQuestionInfo = new PostExamQuestionInfo()
                            {
                                Id = question.Id,
                                Content = question.Content,
                                Explanation = question.Explanation,
                                Type = PostExamQuestionType.WrongAnswer,
                                ListAnswer = new()
                            };
                            foreach (var item in question.Answers)
                            {
                                postExamQuestionInfo.ListAnswer.Add(new PostExamAnswerInfo()
                                {
                                    Id = item.Id,
                                    QuestionId = item.QuestionId,
                                    Content = item.Content,
                                    IsCorrect = item.IsCorrect,
                                    IsChosen = questionAnswers.Any(x => x.AnswerId == item.Id)
                                });
                            };
                            postExam.ListQuestion.Add(postExamQuestionInfo);
                            wrongAnswersNumber++;
                        }
                    }
                    else
                    {
                        PostExamQuestionInfo postExamQuestionInfo = new PostExamQuestionInfo()
                        {
                            Id = question.Id,
                            Content = question.Content,
                            Explanation = question.Explanation,
                            Type = PostExamQuestionType.WrongAnswer,
                            ListAnswer = new()
                        };
                        foreach (var item in question.Answers)
                        {
                            postExamQuestionInfo.ListAnswer.Add(new PostExamAnswerInfo() {
                                Id = item.Id,
                                QuestionId = item.QuestionId,
                                Content = item.Content,
                                IsCorrect = item.IsCorrect,
                                IsChosen = questionAnswers.Any(x => x.AnswerId == item.Id)
                            });
                        };
                        postExam.ListQuestion.Add(postExamQuestionInfo);
                        wrongAnswersNumber++;
                    }
                }
            }

            var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(User.FindFirst(Constants.CLAIM_TYPE).Value);
            var userContest = new UserContest();
            userContest.Id = id;
            userContest.CorrectAnswer= correctAnswersNumber;
            userContest.WrongAnswer= wrongAnswersNumber;
            userContest.NotAnswer= notAnswersNumber;

            DateTime elapsedTime = userContestQuestions.UserContest.StartedTime.Value.AddMinutes(userContestQuestions.UserContest.ContestTime.Value);
            if (DateTime.Compare(DateTime.Now, elapsedTime) > 0)
                userContest.EndedTime = elapsedTime;
            else
                userContest.EndedTime = DateTime.Now;

            userContest.UpdatedTime = DateTime.Now;
            userContest.UpdatedBy = logedInUser?.Id; 
            var responseModel = await _userContestRepository.Update(userContest);

            postExam.UserContest = responseModel.Data;
            ResponseModel result = new ResponseModel() { Status = 1, Message = "OK", Data = postExam };
            return Ok(result);
        }
    }

    public class PostExam
    {
        public UserContestViewModel UserContest { get; set; }
        public List<PostExamQuestionInfo> ListQuestion { get; set; } = new();
    }

    public class PostExamQuestionInfo
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string Explanation { get; set; }
        public PostExamQuestionType Type { get; set; }
        public List<PostExamAnswerInfo> ListAnswer { get; set; }
    }

    public enum PostExamQuestionType
    {
        CorrectAnswer,
        WrongAnswer,
        NotAnswer
    }

    public class PostExamAnswerInfo
    {
        public Guid? Id { get; set; }
        public Guid? QuestionId { get; set; }
        public string? Content { get; set; }
        public bool? IsCorrect { get; set; }
        public bool? IsChosen { get; set; }
    }
}