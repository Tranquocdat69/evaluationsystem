﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        private readonly IUserContestAnswerRepository _userContestAnswerRepository;

        public AnswerController(IUserContestAnswerRepository userContestAnswerRepository)
        {
            _userContestAnswerRepository = userContestAnswerRepository;
        }

        [Role(Constants.CLAIM_TYPE, "GetAnswers")]
        [HttpGet]
        [Route("GetAnswers")]
        public async Task<IActionResult> GetAnswers(Guid userContestId)
        {
            var answers = await _userContestAnswerRepository.GetByUserContestId(userContestId);
            return Ok(answers);
        }

        [Role(Constants.CLAIM_TYPE, "InsertAnswer")]
        [HttpPost]
        [Route("Insert")]
        public async Task<IActionResult> Insert(UserContestAnswer userContestAnswer)
        {
            var response = await _userContestAnswerRepository.Insert(userContestAnswer);
            return Ok(response);
        }

        [Role(Constants.CLAIM_TYPE, "DeleteAnswer")]
        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete(UserContestAnswer userContestAnswer)
        {
            var response = await _userContestAnswerRepository.Delete(userContestAnswer);
            return Ok(response);
        }
    }
}
