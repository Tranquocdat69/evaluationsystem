﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        public FileController(
            IWebHostEnvironment hostEnvironment
        )
        {
            _hostEnvironment = hostEnvironment;
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 4294967295)]
        [RequestSizeLimit(4294967295)]
        [Route("Upload")]
        public async Task<IActionResult> Upload(IFormFile file, string? folder)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                var contentRootPath = _hostEnvironment.ContentRootPath;
                var folderPath = Path.Combine(contentRootPath, "Uploads", folder ?? "");
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                var fileName = file.FileName.Replace(" ", "-");
                string relativePath = "/" + folder + "/" + fileName;
                string path = Path.Combine(contentRootPath, "Uploads", folder, fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
                responseModel.Status = 1;
                responseModel.Message = "Upload file success!";
                responseModel.Data = relativePath;
                return Ok(responseModel);
            }
            catch (Exception ex)
            {
                responseModel.Status = -99;
                responseModel.Message = ex.Message;
                return BadRequest(responseModel);
            }
        }
    }
}
