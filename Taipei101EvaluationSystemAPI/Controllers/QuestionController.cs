﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystem.JwtAuthentication;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly IUserContestQuestionRepository _userContestQuestionRepository;
        private readonly IQuestionRepository _questionRepository;

        public QuestionController(IUserContestQuestionRepository userContestQuestionRepository, IQuestionRepository questionRepository)
        {
            _userContestQuestionRepository = userContestQuestionRepository;
            _questionRepository = questionRepository;
        }

        [Role(Constants.CLAIM_TYPE, "GetAllQuestions")]
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll(Guid userContestId)
        {
            var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(User.FindFirst(Constants.CLAIM_TYPE).Value);
            var questions = await _userContestQuestionRepository.GetAllHasNoIsCorrect(userContestId, logedInUser.Id);
            return Ok(questions);
        }

        [Role(Constants.CLAIM_TYPE, "GetAllQuestions")]
        [HttpGet]
        [Route("GetByQuestionCategory")]
        public async Task<IActionResult> GetByQuestionCategory([FromQuery] int questionCategoryId, int? pageIndex, int? pageSize)
        {
            PagedViewModel responseModel = await _questionRepository.GetByQuestionCategory(questionCategoryId, pageIndex, pageSize);
            return Ok(responseModel);
        }

        [HttpPost]
        [Route("GetRandom")]
        public async Task<IActionResult> GetRandom(List<ContestDetail> listContestDetail)
        {
            ResponseModel responseModel = await _questionRepository.GetRandom(listContestDetail);
            return Ok(responseModel);
        }
    }
}
