using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
using Taipei101EvaluationSystemDAL;
using Taipei101EvaluationSystemDAL.Repositories;
using Taipei101EvaluationSystemDAL.Repositories.ActionRepository;
using Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository;
using Taipei101EvaluationSystemDAL.Repositories.ContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionCategoryRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.RoleActionRepository;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRateRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;

var builder = WebApplication.CreateBuilder(args);
const string CORS_POLICY = "CorsPolicy";
//builder.Services.AddCors(o => o.AddPolicy(CORS_POLICY, builder =>
//{
//    builder.SetIsOriginAllowedToAllowWildcardSubdomains()
//           .AllowAnyOrigin()
//           .AllowAnyMethod()
//           .AllowAnyHeader();
//}));

builder.Services.AddCors(options =>
{
    options.AddPolicy(CORS_POLICY, builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed((host) => true)
                .AllowCredentials());
});

builder.Services.AddSingleton<DapperContext>();

//register interfaces
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IRoleActionRepository, RoleActionRepository>();
builder.Services.AddScoped<IActionRepository, ActionRepository>();
builder.Services.AddScoped<IRoleRepository, RoleRepository>();
builder.Services.AddScoped<IUserContestRepository, UserContestRepository>();
builder.Services.AddScoped<IUserContestQuestionRepository, UserContestQuestionRepository>();
builder.Services.AddScoped<IUserContestAnswerRepository, UserContestAnswerRepository>();
builder.Services.AddScoped<IUserRateRepository, UserRateRepository>();
builder.Services.AddScoped<IContestDetailRepository, ContestDetailRepository>();
builder.Services.AddScoped<IQuestionRepository, QuestionRepository>();
builder.Services.AddScoped<IContestRepository, ContestRepository>();
builder.Services.AddScoped<IContestCategoryRepository, ContestCategoryRepository>();
builder.Services.AddScoped<IQuestionCategoryRepository, QuestionCategoryRepository>();

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Taipei101EvaluationSystem", Version = "v1" });
    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "Example: 'Bearer 12345abcdef",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });

    c.AddSecurityRequirement(
        new OpenApiSecurityRequirement(){
        {
            new OpenApiSecurityScheme
            {
            Reference = new OpenApiReference
                {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header,
            },
            new List<string>()
            }
        });
});

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(builder.Configuration.GetValue<string>("JwtSecurityKey"))),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});

builder.Configuration.AddJsonFile("appsettings.json", false, true);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsProduction())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Taipei101EvaluationSystem v1"));
}

app.UseHttpsRedirection();

var cacheMaxAgeOneWeek = (60 * 60 * 24 * 7).ToString();
var provider = new FileExtensionContentTypeProvider();
app.UseStaticFiles(new StaticFileOptions
{
    OnPrepareResponse = ctx =>
    {
        ctx.Context.Response.Headers.Append("Cache-Control", $"public, max-age={cacheMaxAgeOneWeek}");
        ctx.Context.Response.Headers.Append("Access-Control-Allow-Origin", "*");
        ctx.Context.Response.Headers.Append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    },
    ContentTypeProvider = provider,
    FileProvider = new PhysicalFileProvider(Path.Combine(builder.Environment.ContentRootPath, "Uploads")),
    RequestPath = "/Uploads",
});

app.UseCors(CORS_POLICY);

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
