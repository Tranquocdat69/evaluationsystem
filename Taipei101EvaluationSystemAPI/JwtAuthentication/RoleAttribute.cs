﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Security.Claims;
using Taipei101EvaluationSystemDAL.Repositories.ActionRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystem.JwtAuthentication
{
    public class RoleAttribute : TypeFilterAttribute
    {
        public RoleAttribute(string claimType, string claimValue) : base(typeof(ClaimRequirementFilter))
        {
            Arguments = new object[] { new Claim(claimType, claimValue) };
        }

        public class ClaimRequirementFilter : IAuthorizationFilter
        {
            //got from controllers
            readonly Claim _claim;
            readonly IActionRepository _actionRepository;

            public ClaimRequirementFilter(Claim claim, IActionRepository actionRepository)
            {
                _claim = claim;
                _actionRepository = actionRepository;
            }

            public void OnAuthorization(AuthorizationFilterContext context)
            {
                //this data is set when authenticating
                var claimedData = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == Constants.CLAIM_TYPE);
                if (claimedData != null)
                {
                    var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(claimedData.Value);
                    if (logedInUser.RoleName != Constants.ADMIN_ROLE_NAME)
                    {
                        var actions = _actionRepository.GetByRoleId((Guid)logedInUser.RoleId);
                        
                        if (actions == null || actions.Count == 0 || actions.FirstOrDefault(x => x.ActionName == _claim.Value) == null)
                        {
                            context.Result = new UnauthorizedResult();
                        }
                    }
                }
                else
                {
                    context.Result = new ForbidResult();
                }
            }
        }
    }
}