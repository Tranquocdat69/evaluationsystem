﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taipei101EvaluationSystemDAL.ViewModels;

namespace Taipei101EvaluationSystem.JwtAuthentication
{
    public class JwtAuthResponse
    {
        public string? Token { get; set; }
        public UserViewModel? LogedInUser { get; set; }
        public DateTime Expired { get; set; }
    }
}
