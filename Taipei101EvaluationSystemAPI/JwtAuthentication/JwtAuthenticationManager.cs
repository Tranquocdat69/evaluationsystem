﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystem.JwtAuthentication
{
    public class JwtAuthenticationManager
    {
        private readonly IUserRepository _userRepositories;
        private readonly IConfiguration _configuration;

        public JwtAuthenticationManager(IUserRepository userRepository, IConfiguration configuration)
        {
            _userRepositories = userRepository;
            _configuration = configuration;
        }

        public async Task<ResponseModel> Authenticate(User user)
        {
            var responseModel = await _userRepositories.Login(user);
            if (responseModel.Status == 1)
            {
                var tokenExpiryTimeStamp = DateTime.Now.AddMinutes(Convert.ToDouble(_configuration.GetValue<string>("JwtToValidityMins")));
                var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
                var tokenKey = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JwtSecurityKey"));
                var securityTokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new List<Claim> {
                        new Claim(Constants.CLAIM_TYPE, JsonConvert.SerializeObject(responseModel.Data))
                    }),

                    NotBefore = DateTime.Now,
                    Expires = tokenExpiryTimeStamp,
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
                };

                var securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);
                var token = jwtSecurityTokenHandler.WriteToken(securityToken);

                var jwtAuthResponse = new JwtAuthResponse
                {
                    Token = token,
                    LogedInUser = responseModel.Data,
                    Expired = tokenExpiryTimeStamp
                };
                responseModel.Data = jwtAuthResponse;
            }
            else
            {
                responseModel.Data = null;
            }
            return responseModel;
        }
    }
}