﻿namespace Taipei101EvaluationSystemModels
{
    public class Constants
    {
        public const string CLAIM_TYPE = "LogedInUser";
        public const string ADMIN_ROLE_NAME = "ADMIN";
        public const string SESSION_NAME = "SESSION";
    }
}