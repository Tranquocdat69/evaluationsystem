﻿
function updateDepartment(id) {
    if (id) {
        $.ajax({
            method: 'GET',
            url: '/Department/Update?id=' + id,
            success: function (response) {
                $('#update-department-modal-body').html(response);
                $('#update-department-modal').modal('show');
            }
        })
    }
}

$('#insert-department-btn').click(function () {
    const name = $('#insert-name').val();
    if (name) {
        const department = {
            Name: name
        }
        $.ajax({
            method: 'POST',
            url: '/Department/Insert',
            data: department,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Thêm bộ phận mới thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Bộ phận đã tồn tại!');
                }
                else {
                    console.log(response)
                }
            }
        })
    }
    else {
        toastr.warning('Vui lòng nhập tên bộ phận!')
    }
});

function deleteDepartment(id) {
    if (id) {
        const oke = confirm('Bạn có chắc chắn muốn xóa?');
        if (oke) {
            $.ajax({
                method: 'DELETE',
                url: '/Department/Delete?id=' + id,
                success: function (response) {
                    if (response.status == 1) {
                        toastr.success('Xóa bộ phận thành công!');
                        $('#' + id).remove();
                    }
                    else if (response.status == 0) {
                        toastr.error('Bộ phận không tồn tại!');
                    }
                    else {
                        console.log(response);
                    }
                }
            })
        }
    }
}