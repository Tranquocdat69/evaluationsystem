﻿$(document).ready(function () {
    var firstRole = $('.list-group-item.list-group-item-action.fw-bold.active');
    firstRole.click();
});

function showAllActions(roleId) {
    if (roleId) {
        $.ajax({
            method: 'GET',
            url: '/Action/GetAll?roleId=' + roleId,
            success: function (response) {
                $('#list-action').html(response);
            }
        });
    };
};

function grantActionsToRole() {
    var stringActions = '';
    var roleId = $('#selected-role').val();
    var selectdActions = $('input[name="actions"]:checked');

    for (var i = 0; i < selectdActions.length; i++) {
        if (i == selectdActions.length - 1) {
            stringActions += selectdActions[i].value;
        } else {
            stringActions += selectdActions[i].value + ",";
        }
    };

    var roleAction = {
        RoleId: roleId,
        ActionIds: stringActions
    };

    $.ajax({
        method: 'PUT',
        url: '/RoleAction/Update',
        data: roleAction,
        success: function (response) {
            if (response.status == 1) {
                toastr.success('Phân quyền thành công!');
                setTimeout(() => {
                    location.reload();
                }, 1500)
            }
            else if (response.status == -99) {
                toastr.error('Có lỗi khi phân quyền');
            }
            else {
                console.log(response);
            }
        }
    })
};