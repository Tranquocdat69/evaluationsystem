﻿$('#insert-user-contest-btn').click(function () {
    const userId = $('#insert-user-id').val();
    const contestId = $('#insert-contest-id').val();

    if (userId && contestId) {
        const userContest = {
            UserId: userId,
            ContestId: contestId
        };

        $.ajax({
            method: 'POST',
            url: '/UserContest/Insert',
            data: userContest,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Tạo lịch thi mới thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 2) {
                    toastr.success(`UserContestId: ${response.data.id}`);
                }
                else if (response.status == 0) {
                    toastr.error('Người dùng đang làm bài thi!');
                }
                else {
                    console.log(response)
                }
            }
        });
    } else {
        toastr.error('Vui lòng chọn đầy đủ thông tin!');
    }
});

function deleteUserContest(id) {
    const oke = confirm('Bạn có chắc chắn muốn xóa?');
    if (oke) {
        $.ajax({
            method: 'DELETE',
            url: '/UserContest/Delete?id=' + id,
            success: function (response) {
                if (response.status == 1) {
                    $('#' + id).remove();
                    toastr.success('Xóa lịch thi thành công!');
                }
                else if (response.status == 0) {
                    toastr.error('Lịch thi hiện không tồn tại!');
                }
                else {
                    console.log(response)
                }
            }
        });
    }
}

function getContestResult(userContest) {
    console.log()
}