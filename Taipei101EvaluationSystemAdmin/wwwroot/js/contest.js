﻿
$('#insert-contest-btn').click(function () {
    const name = $('#insert-name').val();
    const time = $('#insert-time').val();
    const level = $('#insert-level').val();
    const questionNumber = $('#insert-question-number').val();

    if (name && time && level && questionNumber) {
        if (time < 1) {
            $('#insert-time').focus();
            toastr.error('Vui lòng nhập thời gian thi lớn hơn 0!');
            return;
        };
        if (level < 1) {
            $('#insert-level').focus();
            toastr.error('Vui lòng nhập cấp độ bài thi lớn hơn 0!');
            return;
        };
        if (questionNumber < 1) {
            $('#insert-question-number').focus();
            toastr.error('Vui lòng nhập số lượng câu hỏi lớn hơn 0!');
            return;
        };
        const contest = {
            Name: name,
            Time: time,
            Level: level,
            QuestionNumber: questionNumber
        };
        $.ajax({
            method: 'POST',
            url: '/Contest/Insert',
            data: contest,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Thêm bài thi mới thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Bài thi đã tồn tại!');
                }
                else {
                    console.log(response)
                }
            }
        })
    }
    else {
        toastr.warning('Vui lòng nhập đủ thông tin!');
    }
});

function updateContest(id) {
    if (id) {
        $.ajax({
            method: 'GET',
            url: '/Contest/Update?id=' + id,
            success: function (response) {
                $('#update-contest-modal-body').html(response);
                $('#update-contest-modal').modal('show');
            }
        })
    }
}

function deleteContest(id) {
    if (id) {
        const oke = confirm('Bạn có chắc chắn muốn xóa?');
        if (oke) {
            $.ajax({
                method: 'DELETE',
                url: '/Contest/Delete?id=' + id,
                success: function (response) {
                    if (response.status == 1) {
                        toastr.success('Xóa bài thi thành công!');
                        $('#' + id).remove();
                    }
                    else if (response.status == 0) {
                        toastr.error('Bài thi không tồn tại!');
                    }
                    else {
                        console.log(response);
                    }
                }
            })
        }
    }
}