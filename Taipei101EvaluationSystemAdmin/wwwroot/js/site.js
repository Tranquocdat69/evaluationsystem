﻿
toastr.options = {
    "positionClass": "toast-top-center",
}

function displayLoading() {
    $('.loading').show();
}
function hideLoading() {
    $('.loading').hide();
}
$(window).on('beforeunload', function () {
    displayLoading();
    setTimeout(function () {
        hideLoading();
    }, 3000)
});

function newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

$(document).ready(function () {
    $('.active a.clickable').on("click", function (e) {
        if ($(this).hasClass('panel-collapsed')) {
            // expand the panel
            $(this).parents('.active').find('.collapsein').slideDown();
            $(this).removeClass('panel-collapsed');
            $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
        else {
            // collapse the panel
            $(this).parents('.active').find('.collapsein').slideUp();
            $(this).addClass('panel-collapsed');
            $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        }
    });
});