﻿$('#insert-evaluarion-criteria-btn').click(function () {
    const criteria = $('#insert-criteria').val();
    const level = $('#insert-level').val();
    const order = $('#insert-order').val();
    if (criteria && level) {
         if (level < 0) {
            toastr.warning('Cấp độ phải lớn hơn hoặc bằng 0!');
            $('#insert-level').focus();
            return;
        }
        if (order && order < 0) {
            toastr.warning('Thứ tự phải lớn hơn hoặc bằng 0!');
            $('#insert-order').focus();
            return;
        }
       
        const evaluationCriteria = {
            Criteria: criteria,
            Level: level,
            Order: order
        };

        $.ajax({
            method: 'POST',
            url: '/EvaluationCriteria/Insert',
            data: evaluationCriteria,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Thêm mới thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Tiêu chí đánh giá đã tồn tại!');
                }
                else {
                    console.log(response)
                }
            }
        });
    } else {
        toastr.warning('Vui lòng nhập đầy đủ thông tin!');
    }
});

function updateEvaluationCriteria(id) {
    if (id) {
        $.ajax({
            method: 'GET',
            url: '/EvaluationCriteria/Update?id=' + id,
            success: function (response) {
                console.log(response)
                $('#update-evaluarion-criteria-modal-body').html(response);
                $('#update-evaluarion-criteria-modal').modal('show');
            }
        })
    }
}

function deleteEvaluationCriteria(id) {
    const oke = confirm('Bạn có chắc chắn muốn xóa?');
    if (oke) {
        if (id) {
            $.ajax({
                method: 'DELETE',
                url: '/EvaluationCriteria/Delete?id=' + id,
                success: function (response) {
                    if (response.status == 1) {
                        toastr.success('Xoá tiêu chí đánh giá thành công!');
                        $('#' + id).remove();
                    }
                    else if (response.status == 0) {
                        toastr.error('Tiêu chí đánh giá hiện không tồn tại!');
                    }
                    else {
                        console.log(response)
                    }
                }
            });
        }
    }
}