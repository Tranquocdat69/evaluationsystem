﻿
toastr.options = {
    "positionClass": "toast-top-center",
}

$('#insert-user-btn').click(function () {
    const avatar = $('#insert-avatar-url').val();
    const department = $('#insert-department').val();
    const username = $('#insert-username').val();
    const password = $('#insert-password').val();
    const name = $('#insert-name').val();
    const role = $('#insert-role').val();

    if (department && username && password && name && role) {
        const user = {
            DepartmentId: department,
            Username: username,
            Password: password,
            Name: name,
            Avatar: avatar,
            RoleId: role
        };
        $.ajax({
            method: 'POST',
            url: '/User/Insert',
            data: user,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Thêm thành viên mới thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Thành viên đã tồn tại!');
                }
                else {
                    console.log(response)
                }
            }
        })
    }
    else {
        toastr.warning('Vui lòng nhập đủ thông tin!');
    }
});

function updateUser(id) {
    if (id) {
        console.log(id)
        $.ajax({
            method: 'GET',
            url: '/User/Update?id=' + id,
            success: function (response) {
                $('#update-user-modal-body').html(response);
                $('#update-user-modal').modal('show');
            }
        })
    }
}

function deleteUser(id) {
    if (id) {
        const oke = confirm('Bạn có chắc chắn muốn xóa!');
        if (oke) {
            $.ajax({
                method: 'DELETE',
                url: '/User/Delete?id=' + id,
                success: function (response) {
                    if (response.status == 1) {
                        toastr.success('Xóa thành viên thành công!');
                        $('#' + id).remove();
                    }
                    else if (response.status == 0) {
                        toastr.error('Thành viên không tồn tại!');
                    }
                    else {
                        console.log(response);
                    }
                }
            })
        }
    }
}

function evaluateUser(level, userId) {
    $.ajax({
        method: 'GET',
        url: '/EvaluationCriteria/Evaluate?level=' + level + '&userId=' + userId,
        success: function (response) {
            $('#evaluate-modal-body').html(response);
            $('#evaluate-modal').modal('show');
        }
    });
}

function watchReview(userId) {
    $.ajax({
        method: 'GET',
        url: '/UserRate/GetByRateFor?userId=' + userId,
        success: function (response) {
            $('#evaluate-modal-body').html(response);
            $('#evaluate-modal').modal('show');
        }
    });
}