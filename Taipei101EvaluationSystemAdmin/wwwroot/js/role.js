﻿$('#insert-role-btn').click(function () {
    const roleName = $('#insert-rolename').val().trim();
    if (roleName) {
        const role = {
            RoleName: roleName,
        };
        $.ajax({
            method: 'POST',
            url: '/Role/Insert',
            data: role,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Thêm vai trò mới thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Vai trò đã tồn tại!');
                }
                else {
                    console.log(response);
                }
            }
        })
    }
    else {
        toastr.warning('Vui lòng nhập tên vai trò!');
        $('#insert-rolename').focus();
    }
});

function openUpdateRoleModal(id) {
    if (id) {
        $.ajax({
            method: 'GET',
            url: '/Role/Update?id=' + id,
            success: function (response) {
                $('#update-role-modal-body').html(response);
                $('#update-role-modal').modal('show');
            }
        })
    }
};

function updateRole(id) {
    const roleName = $('#update-rolename').val().trim();
    if (roleName) {
        const role = {
            Id: id,
            RoleName: roleName,
        };
        $.ajax({
            method: 'PUT',
            url: '/Role/Update',
            data: role,
            success: function (response) {
                if (response.status == 1) {
                    toastr.success('Cập nhật vai trò thành công!');
                    setTimeout(() => {
                        location.reload();
                    }, 1500)
                }
                else if (response.status == 0) {
                    toastr.error('Vai trò đã tồn tại!');
                }
                else {
                    console.log(response);
                }
            }
        })
    }
    else {
        toastr.warning('Vui lòng nhập tên vai trò!');
        $('#update-rolename').focus();
    }
};

function deleteRole(id) {
    if (id) {
        const oke = confirm('Bạn có chắc chắn muốn xóa!');
        if (oke) {
            $.ajax({
                method: 'DELETE',
                url: '/Role/Delete?id=' + id,
                success: function (response) {
                    if (response.status == 1) {
                        toastr.success('Xóa vai trò thành công!');
                        $('#' + id).remove();
                    }
                    else if (response.status == 0) {
                        toastr.error('Vai trò không tồn tại!');
                    }
                    else {
                        console.log(response);
                    }
                }
            })
        }
    }
}