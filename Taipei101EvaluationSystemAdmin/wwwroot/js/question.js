﻿function generateQuestions(contestId) {
    questionNumber = parseInt($('#question-number').val());
    if (contestId && questionNumber) {
        if (questionNumber < 1) {
            toastr.error('Vui lòng nhập số câu hỏi lớn hơn hoặc bằng 1!');
            return;
        }
        if (questionNumber > 10) {
            toastr.warning('Tối đa 10 câu hỏi cho mỗi lần thêm mới!');
            return;
        }
        questions = []
        for (var i = maxOrder + 1; i <= questionNumber + maxOrder; i++) {
            var questionId = newGuid();
            let question = {
                Id: questionId,
                ContestId: contestId,
                Order: i,
                Content: '',
                Answers: [
                    {
                        Id: newGuid(),
                        ContestId: contestId,
                        QuestionId: questionId,
                        Content: '',
                        IsCorrect: false
                    },
                    {
                        Id: newGuid(),
                        ContestId: contestId,
                        QuestionId: questionId,
                        Content: '',
                        IsCorrect: false
                    },
                ]
            };
            questions.push(question);
        }

        generateHtml(questions);
    }
}

function updateQuestionContent(questionId, element) {
    if (questionId) {
        const questionContent = $(element).val();
        questions = questions.map(function (question, index) {
            if (question.Id == questionId) {
                question.Content = questionContent;
            }
            return question;
        });
    }
}

function updateAnswerContent(questionId, answerId, element) {
    if (questionId && answerId) {
        const answerContent = $(element).val();
        questions = questions.map(function (question, index) {
            if (question.Id == questionId) {
                question.Answers = question.Answers.map(function (answer, index) {
                    if (answer.Id == answerId) {
                        answer.Content = answerContent;
                    }
                    return answer;
                })
            }
            return question;
        });
    }
}

function deleteQuestion(questionId) {
    if (questionId) {
        let oke = confirm('Bạn có chắc chắn muốn xóa?');
        if (oke) {
            questions = questions.filter(x => x.Id != questionId);
            let order = maxOrder + 1;
            questions.forEach(function (question, index) {
                question.Order = order;
                order++;
            });

            generateHtml(questions);
        }
    }
}

function createAnswer(contestId, questionId) {
    if (contestId && questionId) {
        questions = questions.map(function (question, index) {
            if (question.Id == questionId) {
                let answer = {
                    Id: newGuid(),
                    ContestId: contestId,
                    QuestionId: questionId,
                    Content: '',
                    IsCorrect: false
                };
                question.Answers.push(answer);
            }
            return question;
        });

        generateHtml(questions);
    }
}

function deleteAnswer(questionId, answerId) {
    if (questionId && answerId) {
        const oke = confirm('Bạn có chắn chắn muốn xóa?');
        if (oke) {
            questions = questions.map(function (question, index) {
                if (question.Id == questionId) {
                    question.Answers = question.Answers.filter(x => x.Id != answerId);
                }
                return question;
            });

            generateHtml(questions);
        }
    }
}

function updateAnswerIsCorrect(questionId, answerId, element) {
    if (questionId && answerId) {
        questions = questions.map(function (question, index) {
            if (question.Id == questionId) {
                question.Answers = question.Answers.map(function (answer, index) {
                    if (answer.Id == answerId) {
                        const checked = $(element).is(':checked');
                        if (checked) {
                            answer.IsCorrect = true;
                        }
                        else {
                            answer.IsCorrect = false;
                        }
                    }
                    return answer;
                })
            }
            return question;
        });
        generateHtml(questions);
    }
}

function insertQuestions() {
    for (var i = 0; i < questions.length; i++) {
        if (questions[i].Content.trim() === '') {
            toastr.error('Vui lòng nhập đủ nội dung cho toàn bộ câu hỏi!');
            return;
        } else {
            for (var j = 0; j < questions[i].Answers.length; j++) {
                if (questions[i].Answers[j].Content.trim() === '') {
                    toastr.error('Vui lòng nhập đủ nội dung cho toàn bộ câu trả lời!');
                    return;
                }
            };
            if (questions[i].Answers.length === 0) {
                toastr.error('Vui lòng thêm vào ít nhất 1 câu trả lời cho mỗi câu hỏi!');
                return;
            }
            var hasCorrectAnswers = questions[i].Answers.some(x => x.IsCorrect == true);
            if (typeof hasCorrectAnswers === 'undefined' || !hasCorrectAnswers) {
                toastr.error('Vui lòng nhập chọn ít nhất 1 câu trả lời đúng cho mỗi câu hỏi!');
                return;
            }
        }
    }
    const questionsList = {
        QuestionViewModels: questions
    };

    $.ajax({
        method: 'POST',
        url: '/Question/InsertMany',
        data: questionsList,
        success: function (response) {
            if (response.status == 1) {
                toastr.success('Thêm câu hỏi mới thành công!');
                setTimeout(() => {
                    location.reload();
                }, 1500)
            }
            else {
                console.log(response)
            }
        }
    });
}

function updateQuestion(id) {
    if (id) {
        $.ajax({
            method: 'GET',
            url: `/Question/Update?id=${id}`,
            success: function (response) {
                $('#update-question-modal-body').html(response);
                $('#update-question-modal').modal('show');
            }
        })
    }
}

function removeQuestion(questionId) {
    if (questionId) {
        const oke = confirm('Bạn có chắc chắn muốn xóa?');
        if (oke) {
            $.ajax({
                method: 'DELETE',
                url: '/Question/Delete?id=' + questionId,
                success: function (response) {
                    if (response.status == 1) {
                        toastr.success('Xoá câu hỏi thành công!');
                        $('#remove-' + questionId).remove();
                    }
                    else if (response.status == 0) {
                        toastr.error('Câu hỏi hiện không tồn tại!');
                    }
                    else {
                        console.log(response)
                    }
                }
            });
        }
    }
}