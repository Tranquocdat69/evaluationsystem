using Taipei101EvaluationSystemDAL;
using Taipei101EvaluationSystemDAL.Repositories.ActionRepository;
using Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository;
using Taipei101EvaluationSystemDAL.Repositories.ContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.DepartmentRepository;
using Taipei101EvaluationSystemDAL.Repositories.EvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionCategoryRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.RoleActionRepository;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserEvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRateRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<DapperContext>();

//register interfaces
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IRoleActionRepository, RoleActionRepository>();
builder.Services.AddScoped<IRoleRepository, RoleRepository>();
builder.Services.AddScoped<IActionRepository, ActionRepository>();
builder.Services.AddScoped<IDepartmentRepository, DepartmentRepository>();
builder.Services.AddScoped<IContestRepository, ContestRepository>();
builder.Services.AddScoped<IQuestionRepository, QuestionRepository>();
builder.Services.AddScoped<IUserContestRepository, UserContestRepository>();
builder.Services.AddScoped<IUserContestQuestionRepository, UserContestQuestionRepository>();
builder.Services.AddScoped<IEvaluationCriteriaRepository, EvaluationCriteriaRepository>();
builder.Services.AddScoped<IUserEvaluationCriteriaRepository, UserEvaluationCriteriaRepository>();
builder.Services.AddScoped<IUserRateRepository, UserRateRepository>();
builder.Services.AddScoped<IQuestionCategoryRepository, QuestionCategoryRepository>();
builder.Services.AddScoped<IContestDetailRepository, ContestDetailRepository>();
builder.Services.AddScoped<IUserContestAnswerRepository, UserContestAnswerRepository>();

// Add services to the container.
builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();

builder.Services.AddSession((option) =>
{
    option.Cookie.Name = "EvaluationSystem";
    option.IdleTimeout = new TimeSpan(1, 0, 0);
});

builder.Configuration.AddJsonFile("appsettings.json", false, true);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseSession();

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
