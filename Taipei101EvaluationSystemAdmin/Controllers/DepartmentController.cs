﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.DepartmentRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class DepartmentController : BaseController
    {
        private readonly IDepartmentRepository _departmentRepository;
        public DepartmentController(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        public async Task<IActionResult> Index(string? name, int pageIndex = 1, int pageSize = 12)
        {
            var departments = await _departmentRepository.GetPaging(name, pageIndex, pageSize);
            ViewBag.Name = name;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(departments);
        }

        [HttpPost]
        public async Task<JsonResult> Insert(Department department)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                department.CreatedBy = logedInUser?.Id;
            }
            var responseModel = await _departmentRepository.Insert(department);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Update(Guid id)
        {
            var department = await _departmentRepository.GetById(id);
            return PartialView(department);
        }

        [HttpPut]
        public async Task<JsonResult> Update(Department department)
        {
            department.UpdatedTime = DateTime.Now;
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                department.UpdatedBy = logedInUser?.Id;
            }
            var responseModel = await _departmentRepository.Update(department);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _departmentRepository.Delete(id);
            return Json(responseModel);
        }
    }
}
