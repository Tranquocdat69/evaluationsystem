﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class UserContestQuestionController : BaseController
    {
        private readonly IUserContestQuestionRepository _userContestQuestionRepository;
        private readonly IQuestionRepository _questionRepository;
        public UserContestQuestionController(IUserContestQuestionRepository userContestQuestionRepository, IQuestionRepository questionRepository)
        {
            _userContestQuestionRepository = userContestQuestionRepository;
            _questionRepository = questionRepository;
        }

        public async Task<IActionResult> Index(Guid userContestId, string? questionContent)
        {
            var questions = await _userContestQuestionRepository.GetByUserContestId(userContestId, questionContent);
            ViewBag.QuestionContent = questionContent;
            return View(questions);
        }

        [HttpPost]
        public async Task<JsonResult> InsertMany(UserContestQuestionsViewModel userContestQuestionsViewModel)
        {
            ResponseModel responseModel = new();
            if (userContestQuestionsViewModel.UserContestQuestions[0].UserContestId.HasValue)
            {
                Guid userContestId = userContestQuestionsViewModel.UserContestQuestions[0].UserContestId.Value;
                UserContestDetailModel userContestDetailModel = await _userContestQuestionRepository.GetByUserContestId(userContestId, null);
                UserContestDetailModel questions = await _userContestQuestionRepository.GetByUserContestId(userContestQuestionsViewModel.UserContestQuestions[0].UserContestId.Value, null);
                if (questions is not null)
                {
                    int currentQuestionNumber = questions.Questions.Count;
                    if (currentQuestionNumber >= userContestDetailModel.UserContest.ContestQuestionNumber)
                    {
                        responseModel.Status = -99;
                        responseModel.Message = "Không thể thêm câu hỏi do vượt quá số câu hỏi cho phép!";
                    }else
                        responseModel = await _userContestQuestionRepository.InsertMany(userContestQuestionsViewModel);
                }
                else
                    responseModel = await _userContestQuestionRepository.InsertMany(userContestQuestionsViewModel);
            }
            else
            {
                responseModel.Status = -99;
                responseModel.Message = "UserContestId không thể null!";
            }

            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid userContestId, Guid questionId)
        {
            var responseModel = await _userContestQuestionRepository.Delete(userContestId, questionId);
            return Json(responseModel);
        }   
    }
}
