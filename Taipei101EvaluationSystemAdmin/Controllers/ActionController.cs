﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystemDAL.Repositories.ActionRepository;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class ActionController : BaseController
    {
        private readonly IActionRepository _actionRepository;

        public ActionController(IActionRepository actionRepository)
        {
            _actionRepository = actionRepository;
        }

        [HttpGet]
        public async Task<PartialViewResult> GetAll(Guid roleId)
        {
            ViewBag.ListActionByRole = _actionRepository.GetByRoleId(roleId);
            List<Taipei101EvaluationSystemDAL.Entities.Action> listAction = await _actionRepository.GetAll("ASC");
            ViewBag.RoleId = roleId;

            return PartialView(listAction);
        }
    }
}
