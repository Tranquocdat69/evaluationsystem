﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.ContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class QuestionController : BaseController
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IConfiguration _configuration;

        public QuestionController(IQuestionRepository questionRepository, IConfiguration configuration)
        {
            _questionRepository = questionRepository;
            _configuration = configuration;
        }

        public async Task<IActionResult> Index(Guid contestId, string? questionContent)
        {
            var data = await _questionRepository.GetByContestId(contestId, questionContent);
            ViewBag.ContestId = contestId;
            ViewBag.QuestionContent = questionContent;
            return View(data);
        }

        public PartialViewResult Generate(QuestionsListViewModel questionsList)
        {
            questionsList.QuestionViewModels?.OrderBy(x => x.Order).ToList();
            return PartialView(questionsList.QuestionViewModels);
        }

        public PartialViewResult GenerateAnswer(Answer answer)
        {
            return PartialView(answer);
        }

        [HttpPost]
        public async Task<JsonResult> InsertMany(QuestionsListViewModel questionsList)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            var createdBy = Guid.NewGuid();
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                createdBy = (Guid)(logedInUser?.Id);
            }
            var responseModel = await _questionRepository.InsertMany(questionsList, createdBy);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Update(Guid id)
        {
            var question = await _questionRepository.GetById(id);
            ViewBag.UploadsFolder = _configuration.GetValue<string>("UploadsFolder");
            return PartialView(question);
        }

        [HttpPut]
        public async Task<JsonResult> Update(QuestionViewModel questionViewModel)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                questionViewModel.UpdatedBy = (Guid)(logedInUser?.Id);
            }
            var responseModel = await _questionRepository.Update(questionViewModel);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _questionRepository.Delete(id);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<IActionResult> Search(Guid? userContestId, Guid contestId, string? content)
        {
            var questions = await _questionRepository.Search(userContestId ,contestId, content);
            if (questions.Count > 0)
            {
                return PartialView(questions);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}