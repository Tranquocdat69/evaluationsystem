﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.DepartmentRepository;
using Taipei101EvaluationSystemDAL.Repositories.EvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserRepository _userRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IConfiguration _configuration;

        public UserController(
            IUserRepository userRepository,
            IDepartmentRepository departmentRepository,
            IRoleRepository roleRepository,
            IConfiguration configuration,
            IEvaluationCriteriaRepository evaluationCriteriaRepository
        )
        {
            _userRepository = userRepository;
            _departmentRepository = departmentRepository;
            _roleRepository = roleRepository;
            _configuration = configuration;
        }

        public async Task<IActionResult> Index(string? searchingText, Guid? departmentId, int pageIndex = 1, int pageSize = 12)
        {
            var users = await _userRepository.GetPaging(searchingText, departmentId, pageIndex, pageSize);
            ViewBag.SearchingText = searchingText;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;

            var departments = await _departmentRepository.GetAll();
            ViewBag.Departments = departments;

            List<Taipei101EvaluationSystemDAL.ViewModels.RoleViewModel> listRoleViewModel = await _roleRepository.GetAll(null);
            ViewBag.Roles = listRoleViewModel;

            ViewBag.UploadsFolder = _configuration.GetValue<string>("UploadsFolder");

            return View(users);
        }

        [HttpPost]
        public async Task<JsonResult> Insert(User user)
        {
            if (!string.IsNullOrEmpty(user.Password))
            {
                var utilities = new Utilities();
                user.Password = utilities.CreateMD5(user.Password);
            }
            var responseModel = await _userRepository.Insert(user);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Update(Guid id)
        {
            var user = await _userRepository.GetById(id);

            ViewBag.UploadsFolder = _configuration.GetValue<string>("UploadsFolder");

            var departments = await _departmentRepository.GetAll();
            ViewBag.Departments = departments;

            List<Taipei101EvaluationSystemDAL.ViewModels.RoleViewModel> roles = await _roleRepository.GetAll(null);
            ViewBag.Roles = roles;

            return PartialView(user);
        }

        [HttpPut]
        public async Task<JsonResult> Update(User user)
        {
            user.UpdatedTime = DateTime.Now;
            if (!string.IsNullOrEmpty(user.Password))
            {
                var utilities = new Utilities();
                user.Password = utilities.CreateMD5(user.Password);
            }
            var responseModel = await _userRepository.Update(user);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _userRepository.Delete(id);
            return Json(responseModel);
        }
    }
}
