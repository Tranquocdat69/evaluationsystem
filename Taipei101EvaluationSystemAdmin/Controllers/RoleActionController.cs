﻿using Microsoft.AspNetCore.Mvc;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.RoleActionRepository;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class RoleActionController : BaseController
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IRoleActionRepository _roleActionRepository;

        public RoleActionController(IRoleRepository roleRepository, IRoleActionRepository roleActionRepository)
        {
            _roleRepository = roleRepository;
            _roleActionRepository = roleActionRepository;
        }

        public async Task<IActionResult> Index()
        {
            List<RoleViewModel> listRole = await _roleRepository.GetAll("ASC");
            return View(listRole);
        }

        [HttpPut]
        public async Task<JsonResult> Update(RoleActionsViewModel roleActionsViewModel)
        {
            var responseModel = await _roleActionRepository.InsertMany(roleActionsViewModel);
            return Json(responseModel);
        }
    }
}
