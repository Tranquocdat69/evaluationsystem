﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}