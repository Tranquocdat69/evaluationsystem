﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.ContestDetailRepository;
using Taipei101EvaluationSystemDAL.Repositories.ContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.QuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestAnswerRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestQuestionRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserContestRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class UserContestController : BaseController
    {
        private readonly IUserContestRepository _userContestRepository;
        private readonly IUserRepository _userRepository;
        private readonly IContestRepository _contestRepository;
        private readonly IUserContestQuestionRepository _userContestQuestionRepository;
        private readonly IContestDetailRepository _contestDetailRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IUserContestAnswerRepository _userContestAnswerRepository;

        public UserContestController(IUserContestRepository userContestRepository, IUserRepository userRepository, IContestRepository contestRepository, IUserContestQuestionRepository userContestQuestionRepository, IContestDetailRepository contestDetailRepository, IQuestionRepository questionRepository, IUserContestAnswerRepository userContestAnswerRepository)
        {
            _userContestRepository = userContestRepository;
            _userRepository = userRepository;
            _contestRepository = contestRepository;
            _userContestQuestionRepository = userContestQuestionRepository;
            _contestDetailRepository = contestDetailRepository;
            _questionRepository = questionRepository;
            _userContestAnswerRepository = userContestAnswerRepository;
        }

        public async Task<IActionResult> Index(Guid? userId, string? contestName, int? type, int pageIndex = 1, int pageSize = 24)
        {
            var userContests = await _userContestRepository.GetPaging(userId, contestName, type, pageIndex, pageSize);
            ViewBag.Users = await _userRepository.GetAll();
            ViewBag.Contests = await _contestRepository.GetAll();
            ViewBag.UserId = userId;
            ViewBag.ContestName = contestName;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(userContests);
        }
        
        [HttpGet]
        public async Task<IActionResult> GetById(Guid id)
        {
            UserContest userContest = await _userContestRepository.GetById(id);
            return Json(userContest);
        }

        [HttpPost]
        public async Task<JsonResult> Insert(UserContest userContest)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                userContest.CreatedBy = logedInUser?.Id;
            }

            var responseModel = await _userContestRepository.Insert(userContest);
            if (responseModel.Status != 0)
            {
                var userContestResponse = (UserContest)responseModel.Data;
                if (responseModel.Status == 1)
                {
                    List<ContestDetail> listContestDetail = await _contestDetailRepository.GetByContestId(userContest.ContestId.Value);
                    var listRandomQuestion = await _questionRepository.GetRandom(listContestDetail);
                    List<Question>? listQuestion = (List<Question>) listRandomQuestion.Data;

                    List<UserContestQuestion> userContestQuestions = new List<UserContestQuestion>();
                    listQuestion.ForEach(x => userContestQuestions.Add(new UserContestQuestion() { UserContestId = userContestResponse.Id, QuestionId = x.Id }));
                    if (userContestQuestions.Count > 0)
                    {
                        UserContestQuestionsViewModel userContestQuestionsViewModel = new UserContestQuestionsViewModel() { UserContestQuestions = userContestQuestions };
                        await _userContestQuestionRepository.InsertMany(userContestQuestionsViewModel);
                    }
                }

                List<UserContestAnswer> listChosenAnswer = await _userContestAnswerRepository.GetByUserContestId(userContestResponse.Id);
                UserContestHasNoIsCorrect userContestQuestion = await _userContestQuestionRepository.GetAllHasNoIsCorrect(userContestResponse.Id, userContest.UserId.Value);
            }
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _userContestRepository.Delete(id);
            return Json(responseModel);
        }
    }
}
