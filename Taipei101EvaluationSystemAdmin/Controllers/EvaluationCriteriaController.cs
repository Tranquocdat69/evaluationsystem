﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.EvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserEvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class EvaluationCriteriaController : Controller
    {
        private readonly IEvaluationCriteriaRepository _evaluationCriteriaRepository;
        private readonly IUserEvaluationCriteriaRepository _userEvaluationCriteriaRepository;
        public EvaluationCriteriaController(IEvaluationCriteriaRepository evaluationCriteriaRepository, IUserEvaluationCriteriaRepository userEvaluationCriteriaRepository)
        {
            _evaluationCriteriaRepository = evaluationCriteriaRepository;
            _userEvaluationCriteriaRepository = userEvaluationCriteriaRepository;
        }

        public async Task<IActionResult> Index(string? criteria, int? level, int pageIndex = 1, int pageSize = 12)
        {
            var evaluationCriterias = await _evaluationCriteriaRepository.GetPaging(criteria, level, pageIndex, pageSize);
            ViewBag.Criteria = criteria;
            ViewBag.Level = level;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(evaluationCriterias);
        }

        [HttpPost]
        public async Task<JsonResult> Insert(EvaluationCriteria evaluationCriteria)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                evaluationCriteria.CreatedBy = logedInUser?.Id;
            }
            var responseModel = await _evaluationCriteriaRepository.Insert(evaluationCriteria);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Update(Guid id)
        {
            var evaluationCriteria = await _evaluationCriteriaRepository.GetById(id);
            return PartialView(evaluationCriteria);
        }

        [HttpPut]
        public async Task<JsonResult> Update(EvaluationCriteria evaluationCriteria)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                evaluationCriteria.UpdatedBy = logedInUser?.Id;
            }
            evaluationCriteria.UpdatedTime = DateTime.Now;
            var responseModel = await _evaluationCriteriaRepository.Update(evaluationCriteria);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _evaluationCriteriaRepository.Delete(id);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Evaluate(int level, Guid userId)
        {
            var evaluationCriterias = await _evaluationCriteriaRepository.GetAll(level);
            ViewBag.UserId = userId;
            var userEvaluationCriterias = await _userEvaluationCriteriaRepository.GetByUserId(userId);
            ViewBag.UserEvaluationCriterias = userEvaluationCriterias;
            return PartialView(evaluationCriterias);
        }
    }
}