﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Taipei101EvaluationSystemDAL.Repositories.UserRateRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class UserRateController : Controller
    {
        private readonly IUserRateRepository _userRateRepository;

        public UserRateController(IUserRateRepository userRateRepository)
        {
            _userRateRepository = userRateRepository;
        }
        [HttpGet]
        public async Task<PartialViewResult> GetByRateFor(Guid userId)
        {
            var userRates = await _userRateRepository.GetByRateFor(userId);
            return PartialView(userRates);
        }
    }
}
