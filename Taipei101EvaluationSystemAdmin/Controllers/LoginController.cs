﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserRepository _userRepository;
        public LoginController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Login(User user)
        {
            var utilities = new Utilities();
            user.Password = utilities.CreateMD5(user.Password);
            var responseModel = await _userRepository.Login(user);

            if (responseModel.Status == 1)
            {
                var logedInUser = (UserViewModel)responseModel.Data;
                if (logedInUser?.RoleName == "Quản lý")
                {
                    var stringAuthModel = JsonConvert.SerializeObject(logedInUser);
                    HttpContext.Session.SetString(Constants.SESSION_NAME, stringAuthModel);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Notification = "Bạn không có quyền truy cập!";
                }
            }
            else if (responseModel.Status == -1)
            {
                ViewBag.Notification = "Mật khẩu không chính xác!";
            }
            else if (responseModel.Status == 0)
            {
                ViewBag.Notification = "Tài khoản không tồn tại!";
            }

            return View("Index");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }
    }
}
