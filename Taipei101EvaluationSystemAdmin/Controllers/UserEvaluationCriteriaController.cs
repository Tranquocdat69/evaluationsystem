﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.EvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserEvaluationCriteriaRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class UserEvaluationCriteriaController : BaseController
    {
        private readonly IUserEvaluationCriteriaRepository _userEvaluationCriteriaRepository;
        public UserEvaluationCriteriaController(IUserEvaluationCriteriaRepository userEvaluationCriteriaRepository)
        {
            _userEvaluationCriteriaRepository = userEvaluationCriteriaRepository;
        }

        [HttpPost]
        public async Task<JsonResult> Insert(UserEvaluationCriteria userEvaluationCriteria)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                userEvaluationCriteria.CreatedBy = logedInUser?.Id;
            }
            var responseModel = await _userEvaluationCriteriaRepository.Insert(userEvaluationCriteria);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid userId, Guid evaluationCriteriaId)
        {
            var responseModel = await _userEvaluationCriteriaRepository.Delete(userId, evaluationCriteriaId);
            return Json(responseModel);
        }
    }
}