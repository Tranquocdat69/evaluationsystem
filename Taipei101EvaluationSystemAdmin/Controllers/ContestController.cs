﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.ContestRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class ContestController : BaseController
    {
        private readonly IContestRepository _contestRepository;
        public ContestController(IContestRepository contestRepository)
        {
            _contestRepository = contestRepository;
        }

        public async Task<IActionResult> Index(string? name, int pageIndex = 1, int pageSize = 8)
        {
            var contests = await _contestRepository.GetPaging(name, pageIndex, pageSize);
            ViewBag.Name = name;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(contests);
        }

        [HttpPost]
        public async Task<JsonResult> Insert(Contest contest)
        {
            contest.CreatedTime= DateTime.Now;
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                contest.CreatedBy = logedInUser?.Id;
            }
            var responseModel = await _contestRepository.Insert(contest);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Update(Guid id)
        {
            var contest = await _contestRepository.GetById(id);
            return PartialView(contest);
        }

        [HttpPut]
        public async Task<JsonResult> Update(Contest contest)
        {
            contest.UpdatedTime = DateTime.Now;
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                contest.UpdatedBy = logedInUser?.Id;
            }
            var responseModel = await _contestRepository.Update(contest);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _contestRepository.Delete(id);
            return Json(responseModel);
        }
    }
}
