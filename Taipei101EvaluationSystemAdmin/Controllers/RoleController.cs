﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Taipei101EvaluationSystemDAL.Entities;
using Taipei101EvaluationSystemDAL.Repositories.RoleRepository;
using Taipei101EvaluationSystemDAL.Repositories.UserRepository;
using Taipei101EvaluationSystemDAL.ViewModels;
using Taipei101EvaluationSystemModels;

namespace Taipei101EvaluationSystemAdmin.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IRoleRepository _roleRepository;

        public RoleController(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<IActionResult> Index()
        {
            List<RoleViewModel> listRole = await _roleRepository.GetAll("ASC");
            return View(listRole);
        }

        [HttpPost]
        public async Task<JsonResult> Insert(Role role)
        {
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                role.CreatedBy = logedInUser.Id;
            }
            var responseModel = await _roleRepository.Insert(role);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> Update(Guid id)
        {
            var role = await _roleRepository.GetById(id);
            return PartialView(role);
        }

        [HttpPut]
        public async Task<JsonResult> Update(Role role)
        {
            role.UpdatedTime = DateTime.Now;
            var session = HttpContext.Session.GetString(Constants.SESSION_NAME);
            if (!string.IsNullOrEmpty(session))
            {
                var logedInUser = JsonConvert.DeserializeObject<UserViewModel>(session);
                role.UpdatedBy = logedInUser.Id;
            }

            var responseModel = await _roleRepository.Update(role);
            return Json(responseModel);
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(Guid id)
        {
            var responseModel = await _roleRepository.Delete(id);
            return Json(responseModel);
        }
    }
}